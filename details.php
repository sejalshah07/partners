<?php

include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}
//include "./lang/$language";
include "./countries.php";
  
if(!aff_check_security())
{
    aff_redirect('index.php');
    exit;
}
    	
include "header.php";?>

<?php global $wpdb;
$affiliates_table_name = WP_AFF_AFFILIATES_TABLE;
$wp_aff_platform_config = WP_Affiliate_Platform_Config::getInstance();
$errorMsg = '';
  
if(isset($_POST['commited']) && $_POST['commited'] == 'yes')
{
	if(!isset($_SESSION['user_id'])){//Check if user is logged in
		die("User is not logged in as an affiliate. Profile update request denied.");
	}

	//Field validation
    if($_POST['clientemail'] == ''){$errorMsg .= AFF_REQUIRED.": ".AFF_EMAIL;}
    if($wp_aff_platform_config->getValue('wp_aff_make_paypal_email_required')=='1'){
    	if($_POST['clientpaypalemail'] == ''){$errorMsg .= AFF_REQUIRED.": ".AFF_PAYPAL_EMAIL;}
    }
    
    if(isset($_POST['seller_request']))
                	$seller_request = $wpdb->escape($_POST['seller_request']);
                else
                	$seller_request = 0;
                	
	     
    if($errorMsg == '')
    {      
    	if(!empty($_POST['password'])){
	    	$password = $_POST['password'];
			include_once(ABSPATH.WPINC.'/class-phpass.php');
			$wp_hasher = new PasswordHash(8, TRUE);
			$password = $wp_hasher->HashPassword($password);
    	}
    	else{
    		$password = $_POST['encrypted-pass'];
    	}	 
    	$payableto = "";//$_POST['clientpayableto']   	 
        $updatedb = "UPDATE $affiliates_table_name SET pass = '".$password."', company = '".$_POST['clientcompany']."', payableto = '".$payableto."', title = '".$_POST['clienttitle']."', firstname = '".$_POST['clientfirstname']."', lastname = '".$_POST['clientlastname']."', email = '".$_POST['clientemail']."', street = '".$_POST['clientstreet']."', town = '".$_POST['clienttown']."', state = '".$_POST['clientstate']."', country = '".$_POST['clientcountry']."', postcode = '".$_POST['clientpostcode']."', website = '".$_POST['webpage']."', phone = '".$_POST['clientphone']."', fax = '".$_POST['clientfax']."', paypalemail = '".$_POST['clientpaypalemail']."', tax_id = '".$_POST['tax_id']."', account_details = '".$_POST['account_details']."', seller_request = ".$seller_request." WHERE refid = '".$_SESSION['user_id']."'";
        
        
        $results = $wpdb->query($updatedb);
		
        do_action('wp_aff_profile_update',$_SESSION['user_id'],$_POST);
		
        echo "<p class='ok'>".AFF_D_CHANGED;
    }
}

$editingaff = $wpdb->get_row("SELECT * FROM $affiliates_table_name WHERE refid = '".$_SESSION['user_id']."'", OBJECT);

if($errorMsg != '')
     echo "<p class='error'>$errorMsg</p>";

if ($editingaff)
{ ?>
<div class="container">

<div class="row">
<div class="table-responsive">

      <h3><a href ="#"">EDIT PROFILE</a></h3>

	<!-- <img src="images/user_signup.png" alt="user details icon" /> -->
 <!--<div id="update_user">-->
      <form action=details.php method=post ENCTYPE=multipart/form-data>
 <table id="reports">
       
        <tr>
          <td><label><?php echo " Partner ID"  ; ?>:</label> <?php //AFF_AFFILIATE_ID ?></td>
          <td><?php echo '<strong>'.$_SESSION['user_id'].'</strong>'; ?>
          </td>
          </tr>
		<tr height="15px"></tr>
         <tr>
         <td> <label><?php echo AFF_PASSWORD; ?>: </label></td>
           <td> <input class="user-edit" type="password" name="password" value=""></td>
         <td>   <input type="hidden" name="encrypted-pass" value="<?php echo $editingaff->pass; ?>"></td>
		</tr>
		<tr height="15px"></tr>
           <tr> <td></td><td><span style="font-size:10px; text-align: right;"><?php echo AFF_LEAVE_EMPTY_TO_KEEP_PASSWORD; ?></span></td>
          </tr>  
          <tr height="15px"></tr>
		<tr>
         <td> <label><?php echo AFF_COMPANY; ?>: </label></td>
         <td>   <input class="user-edit" type=text name=clientcompany value="<?php echo $editingaff->company; ?>"></td>
			</tr>
          <tr height="15px"></tr>
			<tr>	         
			<td> <label><?php echo AFF_TITLE; ?>: </label></td>
               <td> <select class="user-select" name=clienttitle>
                  <option value=Mr <?php if($editingaff->title=="Mr")echo 'selected="selected"';?>><?php echo AFF_MR; ?></option>
                  <option value=Mrs <?php if($editingaff->title=="Mrs")echo 'selected="selected"';?>><?php echo AFF_MRS; ?></option>
                  <option value=Miss <?php if($editingaff->title=="Miss")echo 'selected="selected"';?>><?php echo AFF_MISS; ?></option>
                  <option value=Ms <?php if($editingaff->title=="Ms")echo 'selected="selected"';?>><?php echo AFF_MS; ?></option>
                  <option value=Dr <?php if($editingaff->title=="Dr")echo 'selected="selected"';?>><?php echo AFF_DR; ?></option>
                </select></td>
			</tr>
          <tr height="15px"></tr>
<tr>
        <td>  <label><?php echo AFF_FIRST_NAME; ?>: </label>*</td>
          <td>  <input class="user-edit" type=text name=clientfirstname value="<?php echo $editingaff->firstname; ?>"></td>
</tr>          
<tr height="15px"></tr>
<tr>
        <td>  <label><?php echo AFF_LAST_NAME; ?>: </label>*</td>
           <td> <input class="user-edit" type=text name=clientlastname value="<?php echo $editingaff->lastname; ?>"></td>
</tr>
   <tr height="15px"></tr>       
<tr>         
 <p style = "text-align: center;">
          	<td><label><strong>Seller Status : </strong></label></td>
          <?php 
		echo "<td>";
if($editingaff->seller){ 
		
			echo "<strong>Active</strong>";}
           else{ 
						
				echo "<strong>Inactive</strong>"; 
           		echo "<label><strong>Request Seller Access : </strong></label>";
				echo "</td>";
           		?>
           		<input name = 'seller_request' id = 'seller_request' type = 'checkbox' value ='1' <?php if(isset($_POST['seller_request'])||$editingaff->seller_request){echo "checked";} ?>/>
           		
           		<?php
           		
          } ?> 
</tr>         
 <tr height="15px"></tr>		
<tr>      
 	 
          <td><label><?php echo AFF_EMAIL; ?>: </label>*</td>
            <td><input  type=text name=clientemail  size="25" value="<?php echo $editingaff->email; ?>"></td>
</tr>
 <tr height="15px"></tr>         
<tr>
         <td> <label><?php echo AFF_ADDRESS; ?>: </label></td>
            <td><input class="user-edit" type=text name=clientstreet value="<?php echo $editingaff->street; ?>"></td>
</tr>
  <tr height="15px"></tr>        
<tr>
         <td> <label><?php echo AFF_TOWN; ?>: </label></td>
           <td> <input class="user-edit" type=text name=clienttown value="<?php echo $editingaff->town; ?>"></td>
</tr>
   <tr height="15px"></tr>      
<tr>
         <td> <label><?php echo AFF_STATE; ?>: </label></td>
           <td> <input class="user-edit" type=text name=clientstate value="<?php echo $editingaff->state; ?>"></td>
</tr>
   <tr height="15px"></tr>     
<tr>
         <td> <label><?php echo AFF_COUNTRY; ?>: </label></td>
          <td>  <select class="user-select" name=clientcountry class=dropdown>
                <?php foreach($GLOBALS['countries'] as $key => $country)
                    print '<option value="'.$key.'" '.($editingaff->country == $key ? 'selected' : '').'>'.$country.'</option>'."\n";
                ?>
            </select></td>
</tr>
  <tr height="15px"></tr>        
<tr>
          <td><label><?php echo AFF_ZIP; ?>: </label></td>
         <td>   <input class="user-edit" type=text name=clientpostcode value="<?php echo $editingaff->postcode; ?>"></td>
</tr>
  <tr height="15px"></tr>        
<tr>
        <td>  <label><?php echo AFF_WEBSITE; ?>: </label></td>
            <td><input class="user-edit" type=text name=webpage value="<?php echo $editingaff->website; ?>"></td>
</tr>
  <tr height="15px"></tr>       
<tr>
         <td> <label><?php echo AFF_PHONE; ?>: </label></td>
           <td> <input class="user-edit" type=text name=clientphone value="<?php echo $editingaff->phone; ?>"></td>
</tr>
  <tr height="15px"></tr>        
<tr>
         <td> <label><?php echo AFF_FAX; ?>: </label></td>
            <td><input class="user-edit" type=text name=clientfax value="<?php echo $editingaff->fax; ?>"></td>
</tr>
   <tr height="15px"></tr>      
<tr>
          <td><label><?php echo AFF_PAYPAL_EMAIL; ?>: </label>*</td>
           <td> <input type=text name=clientpaypalemail size="25" value="<?php echo $editingaff->paypalemail; ?>"></td>
</tr>          
<tr height="15px"></tr>
<tr>
	     <td> <label><?php echo AFF_BANK_ACCOUNT_DETAILS; ?>: </label></td>
	      <td>	<textarea name="account_details" cols="23" rows="2"><?php echo $editingaff->account_details; ?></textarea>	 </td>          
</tr>	      
    <tr height="15px"></tr> 
<tr>    
        <td>  <label><?php echo AFF_TAX_ID; ?>: </label></td>
           <td> <input class="user-edit" type=text name=tax_id value="<?php echo $editingaff->tax_id; ?>">   </td> 
</tr>       
    <tr height="15px"></tr>                 
     <tr>  
          
         
          <td>  <input type=hidden name=commited value=yes></td>
            <td><input class="button" type=submit name=Submit value="<?php echo AFF_UPDATE_BUTTON_TEXT; ?>"></td>
         
</tr>
        
        </table>
      </form>


</div>
</div>
</div>


<?php } ?>

<?php include "footer.php"; ?>
