<?php
include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}
//include "./lang/$language";
if(!aff_check_security())
{
    aff_redirect('index.php');
    exit;
}

include "header.php"; 
 ?>
<!-- <p styel="font-size:12px;"><strong>HOME</strong</p> -->
<!-- <h3><a href"#" style="float:left;">HOME</a></h3>-->


<!-- <img src="images/wp_aff_stats.jpg" alt="Stats Icon" /> hide image of stat on login page -->


 <div class="container">

<div class="row">
<div class="table-responsive">
 <!--
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="text-center">
-->
<?php $affiliates_clickthroughs_table = WP_AFF_CLICKTHROUGH_TABLE;
$sales_table = WP_AFF_SALES_TABLE;
$affiliates_table_name = WP_AFF_AFFILIATES_TABLE;

wp_aff_show_stats();

include "footer.php";

function wp_aff_show_stats()
{
	global $wpdb,$affiliates_table_name;
	$wp_aff_affiliates_db = $wpdb->get_row("SELECT * FROM $affiliates_table_name WHERE refid = '".$_SESSION['user_id']."'", OBJECT);
	echo '<h3>'.AFF_WELCOME.' '.$wp_aff_affiliates_db->firstname.'</h3>';
	if($wp_aff_affiliates_db->seller)
		echo "<h4> (Seller Status: Active)</h4>";

	$default_landing_page = get_option('wp_aff_default_affiliate_landing_url');
	if(empty($default_landing_page))
	{
		$default_affiliate_home_url = home_url();
	}
	else
	{
		$default_affiliate_home_url = $default_landing_page;
	}
	$separator='?';
	$url = $default_affiliate_home_url;
	if(strpos($url,'?')!==false) 
	{
		$separator='&';
	}
	$aff_url = $url.$separator.'aff_id='.$_SESSION['user_id'];	
	$affiliate_link = '<a href="'.$aff_url.'" target="_blank">'.$aff_url.'</a>';	
	//echo '<strong>'.AFF_AFFILIATE_ID.': '.$_SESSION['user_id'].'</strong><br /><br />';
	echo '<strong>Partner ID: '.$_SESSION['user_id'].'</strong><br /><br />';
	echo '<strong>'.AFF_YOUR_AFF_LINK.$affiliate_link.'</strong>';	
	echo "<br />";	
	
	//Welcome message
	$wp_aff_platform_config = WP_Affiliate_Platform_Config::getInstance();
	$wp_aff_welcome_page_msg = $wp_aff_platform_config->getValue('wp_aff_welcome_page_msg');
	if(!empty($wp_aff_welcome_page_msg)){
		$wp_aff_welcome_page_msg = html_entity_decode($wp_aff_welcome_page_msg, ENT_COMPAT, "UTF-8");
		echo '<div class="wp_aff_welcome_page_msg">'.$wp_aff_welcome_page_msg.'</div>';
	}
		
    //commenting, if you uncomment, make sure you troubleshoot show_stats_betweeb_dates carefully!!!! karthik feb 2014
    //include ("reports.php");

	if (isset($_POST['info_update']))
    {
    	$start_date = (string)$_POST["start_date"];
    	$end_date = (string)$_POST["end_date"];
        echo '<h4>';
        echo AFF_STATS_OVERVIEW_BETWEEN.' <font style="color:#222">'.$start_date.'</font> '.AFF_AND.' <font style="color:#222">'. $end_date;
      
        echo '</font></h4>';

        show_stats_between_dates($start_date,$end_date);
	//show_deal_vendor_stats_between_dates($start_date,$end_date);
    }
    else
    {
		/*$curr_date = (date ("Y-m-d"));
		$m = date('m');
		$y = date('Y');*/
		//$start_date = $y.'-'.$m.'-01';
		$end_date = $curr_date;
		

	    echo '<h4>';
	   // echo AFF_STATS_OVERVIEW;
	    echo "<br>Partner Stats Overview";
	    echo '</h4>';

		show_alltime_stats();
		//show_deal_vendor_stats_between_dates($start_date,$end_date);
    }
}

/* -------- Changes made by Dinesh on 14th November, 2013 -------------*/

function show_alltime_stats()
{
	global $wpdb;
	global $affiliates_clickthroughs_table;
	global $sales_table;
	global $affiliates_table_name;
	
	

	$query = $wpdb->get_row("SELECT count(*) as total_record FROM $affiliates_clickthroughs_table  WHERE refid = '".$_SESSION['user_id']."'", OBJECT);
	
	$total_clicks = $query->total_record;
	if (empty($total_clicks))
	{
		$total_clicks = "0";
	}
	
	$query = $wpdb->get_row("SELECT count(*) as total_record FROM $sales_table WHERE payment > 0 AND refid = '".$_SESSION['user_id']."' AND type = 'affiliate'", OBJECT);
	$number_of_sales = $query->total_record;
	if (empty($number_of_sales))
	{
		$number_of_sales = "0";
	}
		
	$row = $wpdb->get_row("select SUM(sale_amount) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND type = 'affiliate'", OBJECT);
	$total_sales = $row->total;
	if (empty($total_sales))
	{
		$total_sales = "0.00";
	}
	
	$row = $wpdb->get_row("select SUM(payment) AS total from `wp_affiliates_sales_tbl` aff, `wp_getdpd_sales` sales WHERE aff.txn_id = sales.id AND sales.status = 'ACT' AND refid = '".$_SESSION['user_id']."' AND type = 'affiliate'", OBJECT);
	//echo "SELECT SUM(payment) AS total from $affiliates_table_name aff, `wp_getdpd_sales` sales WHERE aff.txn_id = sales.id AND sales.status = 'ACT' AND refid = '".$_SESSION['user_id']."' AND type = 'affiliate'";
	$total_commission_aff = $row->total;
	
	//manual aff commissions
	$aff_row = $wpdb->get_row("select SUM(payment) AS total from `wp_affiliates_sales_tbl` aff where aff.txn_id = 'manual' AND refid = '".$_SESSION['user_id']."' AND type = 'affiliate'", OBJECT);
	$total_commission_aff_manual = ($aff_row->total != '' ? $aff_row->total : '0');
	
	$total_commission_aff = $total_commission_aff + $total_commission_aff_manual ;
	
	if (empty($total_commission_aff))
	{
		$total_commission_aff = "0.00";
	}
	
	$wp_aff_affiliates_db = $wpdb->get_row("SELECT * FROM $affiliates_table_name WHERE refid = '".$_SESSION['user_id']."'", OBJECT);
	$commission_level = $wp_aff_affiliates_db->commissionlevel;
	
	$currency = get_option('wp_aff_currency');
	echo '<div id = "aff_tab"><h3> Affiliates Sales Data</h3>
	<table id="reports" width="500">
	<tbody>';

		echo '<tr>';
		echo '<td><strong>'.AFF_TOTAL_CLICKS.' : </strong></td>';
		echo '<td>'.$total_clicks.'</td>';
		echo '</tr>';

		echo '<tr>';
		//echo '<td><strong>'.AFF_NUMBER_OF_SALES.' : </strong></td>';
		echo '<td><strong>Number of Affiliate Sales : </strong></td>';
		echo '<td>'.$number_of_sales.'</td>';
		echo '</tr>';

		echo '<tr>';
		//echo '<td><strong>'.AFF_TOTAL_SALES_AMOUNT.' : </strong></td>';
		echo '<td><strong>Total Affiliate Sales Amount : </strong></td>';
		echo '<td>'.number_format($total_sales,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Total Affiliate Commission Earned : </strong></td>';
		echo '<td>'.number_format($total_commission_aff,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		if (get_option('wp_aff_use_2tier'))
		{
			$second_tier_commission_level = $wp_aff_affiliates_db->sec_tier_commissionlevel;
			if(empty($second_tier_commission_level)){
				$second_tier_commission_level = get_option('wp_aff_2nd_tier_commission_level');
			}
			echo '<tr>';
			echo '<td><strong>'.AFF_2ND_TIER_COMMISSION_LEVEL.' : </strong></td>';
			echo '<td>'.$second_tier_commission_level.'</td>';
			
	        if (get_option('wp_aff_use_fixed_commission'))
			{
	            echo '<td>'.$currency.'</td>';
	        }
	        else
	        {
	            echo '<td>%</td>';
	        }
			echo '</tr>';	
		}	
								
	echo '</tbody></table></div>';
	
	/* Querries for Deals Sales Data Table */
	
	 $start_date_tstamp = new DateTime($start_date);
$end_date_tstamp = new DateTime($end_date." 23:59:59");

	$query = $wpdb->get_row("SELECT count( 1 ) AS total_record
				FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
				WHERE sales.status = 'ACT' AND
				    lineitems.`purchase_id` = sales.`id` AND
				    lineitems.`seller_name` = '".$_SESSION['user_id']."' AND lineitems.`total` > 0");
	
	$number_of_sales = $query->total_record;
	if (empty($number_of_sales))
	{
		$number_of_sales = "0";
	}
		
	//$row = $wpdb->get_row("select SUM(sale_amount) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'", OBJECT);
	
	$row = $wpdb->get_row("SELECT SUM( lineitems.`total` ) AS total
				FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
				WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
				lineitems.`seller_name` = '".$_SESSION['user_id']."'", OBJECT);
	
	$total_sales = $row->total;
	if (empty($total_sales))
	{
		$total_sales = "0.00";
	}
	
	$row = $wpdb->get_row(" SELECT SUM( sales.`processor_fee` ) / 2 AS charges
				FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
				WHERE lineitems.`purchase_id` = sales.`id` AND
				    lineitems.`total` > 0 AND
				    sales.status = 'ACT' AND
				    lineitems.`payment_to` = 'DealFuel' AND
				    lineitems.`seller_name` = '".$_SESSION['user_id']."'", OBJECT);					
					
	$total_sales_charges = $row->charges;
	if (empty($total_sales_charges))
	{
		$total_sales_charges = "0.00";
	}
	    
	//$total_sales_charges = 0;
	    
	//$row = $wpdb->get_row("select SUM(payment) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'", OBJECT);
	    
	$row = $wpdb->get_row("SELECT SUM( lineitems.`seller_comm` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.`purchase_id` = sales.`id` AND
			      lineitems.`total` > 0 AND
			      sales.status = 'ACT' AND			      
			      lineitems.`seller_name` = '".$_SESSION['user_id']."'", OBJECT);
	    
	$total_commission = $row->total;
	    
	$total_commission_net = $total_commission - $total_sales_charges;
	    
	if (empty($total_commission))
	{
		$total_commission = "0.00";
	}
	
	
	$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND `payout_payment` > 0 ", OBJECT);
	
	$total_payments_D2V = $row->total;
	
	$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND `payout_payment` < 0", OBJECT);
	
	$total_payments_V2D = $row->total;
	
	//$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date'", OBJECT);
        $row = $wpdb->get_row("SELECT SUM( lineitems.`total` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
				lineitems.`seller_name` = '".$_SESSION['user_id']."' AND lineitems.`payment_to` = 'Vendor'", OBJECT);

	$total_payments_C2V = $row->total;
	
	//$total_payments_net =   $total_commission + $total_commission_aff - $total_payments_D2V - $total_payments_C2V + (-1) * $total_payments_V2D;
	$total_payments_net =   $total_commission_net + $total_commission_aff - $total_payments_D2V - $total_payments_C2V + (-1) * $total_payments_V2D;
	
	
	/* added by Sneha on 3 July : To calculate current month due*/
	
	$start_date = "2011-01-01";
	$start_date_tstamp = new DateTime($start_date);
	
	//$month_start=date('Y-m-d', strtotime(date('Y-m')." -1 month"));
	$end_date_cur=date('Y-m-d H:i:s', strtotime(date('Y-m')." -1 month -1day 23:59:59"));
	$end_date_tstamp = new DateTime($end_date_cur);
	
	//echo $end_date_cur;
	
	$row = $wpdb->get_row("SELECT SUM( lineitems.`seller_comm` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.total > 0 AND
			      sales.status = 'ACT' AND
			      lineitems.`purchase_id` = sales.`id` AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
				(sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." 
				AND ".$end_date_tstamp->getTimestamp().")", OBJECT);
	
	
	$sales_commission_cur = ($row->total != '' ? $row->total : '0');
	//echo "sales_comm".$sales_commission_cur;
	$row = $wpdb->get_row("SELECT SUM( sales.`processor_fee`  ) / 2 AS C2D_charges
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.total > 0 AND
			      sales.status = 'ACT' AND
			      lineitems.`payment_to` = 'DealFuel' AND
			      lineitems.`purchase_id` = sales.`id` AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
				(sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND 
			".$end_date_tstamp->getTimestamp().")", OBJECT);
	
	$sales_commission_charges_cur = ($row->C2D_charges != '' ? $row->C2D_charges : '0');
	//echo $sales_commission_charges_cur;
	$sales_net_cur = $sales_commission_cur - $sales_commission_charges_cur;
	//echo $sales_net_cur;
	$total_payments_net_cur =   $sales_net_cur + $total_commission_aff - $total_payments_D2V - $total_payments_C2V + (-1) * $total_payments_V2D;
	//echo $total_payments_net_cur;
	//$paypal_masspay_charge=($total_payments_net_cur * (1.96/100))<=50 ? ($total_payments_net_cur * (1.96/100)) : 50;
	if($total_payments_net_cur>0)
		$paypal_masspay_charge=($total_payments_net_cur * (1.96/100))<=50 ? ($total_payments_net_cur * (1.96/100)) : 50;
	else
		$paypal_masspay_charge=0;
	$net_cur_masspay= $total_payments_net_cur - $paypal_masspay_charge;
	
	/*end of adding Sneha*/
	
	echo '<div id = "sales_tab"><h3>Deals Sales Data</h3>
	<table id="reports" width="500">
	<tbody>';

		/*echo '<tr>';
		echo '<td><strong>'.AFF_TOTAL_CLICKS.' : </strong></td>';
		echo '<td>'.$total_clicks.'</td>';
		echo '</tr>';*/

		echo '<tr>';
		echo '<td><strong>Number of Product Sales : </strong></td>';
		echo '<td>'.$number_of_sales.'</td>';
		echo '</tr>';

		echo '<tr>';
		echo '<td><strong>Total Product Sales Amount : </strong></td>';
		echo '<td>$'.number_format($total_sales,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Paypal Fees : </strong></td>';
		echo '<td>$'.number_format($total_sales_charges,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Total Product Commission Earned : </strong></td>';
		//echo '<td>$'.number_format($total_commission,2).'</td>';
		echo '<td>$'.number_format($total_commission_net,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>DealFuel to '.$_SESSION['user_id'].' Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_D2V,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>'.$_SESSION['user_id'].' to Dealfuel Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_V2D,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Customer to '.$_SESSION['user_id'].' Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_C2V,2).'</td>'; 
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
                
		echo '<tr>';
		echo '<td colspan="3"><h3>Payment Due - Sales and Affilate referrals<h3></td>';
		//echo '<td></td>';
		//echo '<td></td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Total payment Due</strong></td>';
		echo '<td>$'.number_format($total_payments_net,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Payment to be made this month</strong></td>';
		echo '<td><strong>$'.number_format($net_cur_masspay,2).'</strong></td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		/*echo '<td><strong>'.AFF_COMMISSION_LEVEL.' : </strong></td>';
		echo '<td>'.$commission_level.'</td>';*/
		
              /*  if (get_option('wp_aff_use_fixed_commission'))
		{
                    echo '<td>'.$currency.'</td>';
                }
                else
                {
                    echo '<td>%</td>';
                }*/
		echo '</tr>';
		
		if (get_option('wp_aff_use_2tier'))
		{
			$second_tier_commission_level = $wp_aff_affiliates_db->sec_tier_commissionlevel;
			if(empty($second_tier_commission_level)){
				$second_tier_commission_level = get_option('wp_aff_2nd_tier_commission_level');
			}
			echo '<tr>';
			echo '<td><strong>'.AFF_2ND_TIER_COMMISSION_LEVEL.' : </strong></td>';
			echo '<td>'.$second_tier_commission_level.'</td>';
			
	        if (get_option('wp_aff_use_fixed_commission'))
			{
	            echo '<td>'.$currency.'</td>';
	        }
	        else
	        {
	            echo '<td>%</td>';
	        }
			echo '</tr>';	
		}	
								
	echo '</tbody></table></div>';

}



/*------------- End By Dinesh On 14th November, 2013 ------------------*/



function show_stats_between_dates($start_date,$end_date)
{
	global $wpdb;
	global $affiliates_clickthroughs_table;
	global $sales_table;
	global $affiliates_table_name;
	
	

	$query = $wpdb->get_row("SELECT count(*) as total_record FROM $affiliates_clickthroughs_table  WHERE refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date'", OBJECT);
	
	$total_clicks = $query->total_record;
	if (empty($total_clicks))
	{
		$total_clicks = "0";
	}
	
	$query = $wpdb->get_row("SELECT count(*) as total_record FROM $sales_table WHERE payment > 0 AND refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'affiliate'", OBJECT);
	$number_of_sales = $query->total_record;
	if (empty($number_of_sales))
	{
		$number_of_sales = "0";
	}
		
		
		
		
	$row = $wpdb->get_row("select SUM(sale_amount) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'affiliate'", OBJECT);
	$total_sales_aff = $row->total;
	
	if (empty($total_sales_aff))
	{
		$total_sales_aff = "0.00";
	}

	$row = $wpdb->get_row("select SUM(payment) AS total from `wp_affiliates_sales_tbl` aff, `wp_getdpd_sales` sales
			      WHERE aff.txn_id = sales.id AND sales.status = 'ACT' AND refid = '".$_SESSION['user_id']."'
			      AND (date BETWEEN '$start_date' AND '$end_date') AND type = 'affiliate'", OBJECT);
	//echo "SELECT SUM(payment) AS total from $affiliates_table_name aff, `wp_getdpd_sales` sales WHERE aff.txn_id = sales.id AND sales.status = 'ACT' AND refid = '".$_SESSION['user_id']."' AND type = 'affiliate'";
	$total_commission_aff = $row->total;
	
	//manual aff commissions
	$aff_row = $wpdb->get_row("select SUM(payment) AS total from `wp_affiliates_sales_tbl` aff
				  where aff.txn_id = 'manual' AND refid = '".$_SESSION['user_id']."'
				  AND (date BETWEEN '$start_date' AND '$end_date' ) AND type = 'affiliate'", OBJECT);
	$total_commission_aff_manual = ($aff_row->total != '' ? $aff_row->total : '0');
	
	$total_commission_aff = $total_commission_aff + $total_commission_aff_manual ;

	

	$wp_aff_affiliates_db = $wpdb->get_row("SELECT * FROM $affiliates_table_name WHERE refid = '".$_SESSION['user_id']."'", OBJECT);
	$commission_level = $wp_aff_affiliates_db->commissionlevel;
	
	$currency = get_option('wp_aff_currency');
	echo '<div id = "aff_tab"><h3> Affiliates Sales Data</h3>
	<table id="reports" width="300">
	<tbody>';

		echo '<tr>';
		echo '<td><strong>'.AFF_TOTAL_CLICKS.' : </strong></td>';
		echo '<td>'.$total_clicks.'</td>';
		echo '</tr>';

		echo '<tr>';
		//echo '<td><strong>'.AFF_NUMBER_OF_SALES.' : </strong></td>';
		echo '<td><strong>Number of Affiliate Sales : </strong></td>';
		echo '<td>'.$number_of_sales.'</td>';
		echo '</tr>';

		echo '<tr>';
		//echo '<td><strong>'.AFF_TOTAL_SALES_AMOUNT.' : </strong></td>';
		echo '<td><strong>Total Affiliate Sales Amount : </strong></td>';
		echo '<td>'.number_format($total_sales_aff,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Total Affiliate Commission Earned : </strong></td>';
		echo '<td>'.number_format($total_commission_aff,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		

		echo '<tr>';
		/*echo '<td><strong>'.AFF_COMMISSION_LEVEL.' : </strong></td>';
		echo '<td>'.$commission_level.'</td>';*/
		
              /*  if (get_option('wp_aff_use_fixed_commission'))
		{
                    echo '<td>'.$currency.'</td>';
                }
                else
                {
                    echo '<td>%</td>';
                }*/
		echo '</tr>';
		
		if (get_option('wp_aff_use_2tier'))
		{
			$second_tier_commission_level = $wp_aff_affiliates_db->sec_tier_commissionlevel;
			if(empty($second_tier_commission_level)){
				$second_tier_commission_level = get_option('wp_aff_2nd_tier_commission_level');
			}
			echo '<tr>';
			echo '<td><strong>'.AFF_2ND_TIER_COMMISSION_LEVEL.' : </strong></td>';
			echo '<td>'.$second_tier_commission_level.'</td>';
			
	        if (get_option('wp_aff_use_fixed_commission'))
			{
	            echo '<td>'.$currency.'</td>';
	        }
	        else
	        {
	            echo '<td>%</td>';
	        }
			echo '</tr>';	
		}	
								
	echo '</tbody></table></div>';
	
	/* Querries for Deals Sales Data Table */
	
	 $start_date_tstamp = new DateTime($start_date);
//echo "start:".$start_date_tstamp->getTimestamp();
//echo "--";
$end_date_tstamp = new DateTime($end_date." 23:59:59");
//echo "end: ".$end_date_tstamp->getTimestamp();
	
	/*$query = $wpdb->get_row("SELECT count(*) as total_record FROM $affiliates_clickthroughs_table  WHERE refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date'", OBJECT);
	
	$total_clicks = $query->total_record;
	if (empty($total_clicks))
	{
		$total_clicks = "0";
	}
	*/
	//$query = $wpdb->get_row("SELECT count(*) as total_record FROM $sales_table WHERE payment > 0 AND refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'" , OBJECT);
	
	
	$query = $wpdb->get_row("SELECT count( 1 ) AS total_record
				FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
				WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND				
				lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
				sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp()." AND sales.`total` > 0");
	
	
	
	$number_of_sales = $query->total_record;
	if (empty($number_of_sales))
	{
		$number_of_sales = "0";
	}
		
	//$row = $wpdb->get_row("select SUM(sale_amount) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'", OBJECT);
	
	$row = $wpdb->get_row("SELECT SUM( lineitems.`total` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
			      sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp(), OBJECT);
	
	$total_sales = $row->total;
	if (empty($total_sales))
	{
		$total_sales = "0.00";
	}
	
	//$row = $wpdb->get_row("select SUM(payment) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'", OBJECT);
	
	$row = $wpdb->get_row("SELECT SUM( lineitems.`seller_comm` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
			      sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp(), OBJECT);
	
	$total_commission = $row->total;
	if (empty($total_commission))
	{
		$total_commission = "0.00";
	}

	
	$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND (date BETWEEN '$start_date' AND '$end_date') AND `payout_payment` > 0 ", OBJECT);
	
	$total_payments_D2V = $row->total;
	
	$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND (date BETWEEN '$start_date' AND '$end_date' ) AND `payout_payment` < 0", OBJECT);
	
	$total_payments_V2D = $row->total;
	
	//$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date'", OBJECT);
        $row = $wpdb->get_row("SELECT SUM( lineitems.`total` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
			      lineitems.`payment_to` = 'Vendor' AND
			      sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp(), OBJECT);

	$total_payments_C2V = $row->total;
	
	$total_payments_net =   $total_commission + $total_commission_aff - $total_payments_D2V - $total_payments_C2V + (-1) * $total_payments_V2D;
	
	
	echo '<div id = "sales_tab" class="pull-left"><h3>Deals Sales Data</h3>
	<table id="reports" width="300">
	<tbody>';

		/*echo '<tr>';
		echo '<td><strong>'.AFF_TOTAL_CLICKS.' : </strong></td>';
		echo '<td>'.$total_clicks.'</td>';
		echo '</tr>';*/

		echo '<tr>';
		echo '<td><strong>Number of Product Sales : </strong></td>';
		echo '<td>'.$number_of_sales.'</td>';
		echo '</tr>';

		echo '<tr>';
		echo '<td><strong>Total Product Sales Amount : </strong></td>';
		echo '<td>$'.number_format($total_sales,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Total Product Commission Earned : </strong></td>';
		echo '<td>$'.number_format($total_commission,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>DealFuel to '.$_SESSION['user_id'].' Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_D2V,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>'.$_SESSION['user_id'].' to Dealfuel Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_V2D,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Customer to '.$_SESSION['user_id'].' Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_C2V,2).'</td>'; 
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
                
		echo '<tr>';
		echo '<td><h3>Net Payment Due<h3></td>';
		echo '<td></td>';
		echo '<td></td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Net Due from Sales and Affilate referrals</strong></td>';
		echo '<td>$'.number_format($total_payments_net,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		/*echo '<td><strong>'.AFF_COMMISSION_LEVEL.' : </strong></td>';
		echo '<td>'.$commission_level.'</td>';*/
		
              /*  if (get_option('wp_aff_use_fixed_commission'))
		{
                    echo '<td>'.$currency.'</td>';
                }
                else
                {
                    echo '<td>%</td>';
                }*/
		echo '</tr>';
		
		if (get_option('wp_aff_use_2tier'))
		{
			$second_tier_commission_level = $wp_aff_affiliates_db->sec_tier_commissionlevel;
			if(empty($second_tier_commission_level)){
				$second_tier_commission_level = get_option('wp_aff_2nd_tier_commission_level');
			}
			echo '<tr>';
			echo '<td><strong>'.AFF_2ND_TIER_COMMISSION_LEVEL.' : </strong></td>';
			echo '<td>'.$second_tier_commission_level.'</td>';
			
	        if (get_option('wp_aff_use_fixed_commission'))
			{
	            echo '<td>'.$currency.'</td>';
	        }
	        else
	        {
	            echo '<td>%</td>';
	        }
			echo '</tr>';	
		}	
								
	echo '</tbody></table></div>';

}

function show_deal_vendor_stats_between_dates($start_date,$end_date){

	global $wpdb;
	
	$currency = get_option('wp_aff_currency');
		
	/* Querries for Payment made to Table */
	
	 $start_date_tstamp = new DateTime($start_date);
//echo "start:".$start_date_tstamp->getTimestamp();
//echo "--";
$end_date_tstamp = new DateTime($end_date." 23:59:59");
//echo "end: ".$end_date_tstamp->getTimestamp();
	$sql = "SELECT count( 1 ) AS total_record
	    FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
	    WHERE lineitems.`purchase_id` = sales.`id` AND
		lineitems.`total` > 0 AND
		sales.status = 'ACT' AND
	    lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
	    (sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp().") AND
	    sales.`total` > 0 AND lineitems.`payment_to` =  'DealFuel'" ;
	
	$query = $wpdb->get_row($sql, OBJECT);
	
	
	//print date("Y-M-d  H:i:s","1383264000");
	//print date("Y-M-d  H:i:s","1383868799");
	
	$number_of_sales = $query->total_record;
	if (empty($number_of_sales))
	{
		$number_of_sales = "0";
	}
		
	//$row = $wpdb->get_row("select SUM(sale_amount) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'", OBJECT);
	$sql = "SELECT SUM( lineitems.`total` ) AS total
		FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
		WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
		lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
		(sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp().") AND
		lineitems.`payment_to` = 'DealFuel' " ;
		//echo $sql;
	$row = $wpdb->get_row($sql, OBJECT);
	
	$total_sales = $row->total;
	if (empty($total_sales))
	{
		$total_sales = "0.00";
	}
	
	//$row = $wpdb->get_row("select SUM(payment) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'", OBJECT);
	
	$row = $wpdb->get_row("SELECT SUM( lineitems.`seller_comm` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
			      (sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp() .") AND
			      lineitems.`payment_to` = 'DealFuel'", OBJECT);
	
	$total_commission = $row->total;
	if (empty($total_commission))
	{
		$total_commission = "0.00";
	}

          $df_commissions = $total_sales-$total_commission;
	
	
	
	
	echo '<h3>Payment Made to DealFuel</h3>
	<table id="reports" width="300">
	<tbody>';

		/*echo '<tr>';
		echo '<td><strong>'.AFF_TOTAL_CLICKS.' : </strong></td>';
		echo '<td>'.$total_clicks.'</td>';
		echo '</tr>';*/

		echo '<tr>';
		echo '<td><strong>Total Number of Sales : </strong></td>';
		echo '<td>'.$number_of_sales.'</td>';
		echo '</tr>';

		echo '<tr>';
		echo '<td><strong>Total Sales Amount : </strong></td>';
		echo '<td>$'.number_format($total_sales,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>DealFuel Commission : </strong></td>';
		echo '<td>$'.number_format($df_commissions,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>'.$_SESSION['user_id'].' Commissions : </strong></td>';
		echo '<td>$'.number_format($total_commission,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';

								
	echo '</tbody></table>';


//    ================ Vendor Table ==========================

	$query = $wpdb->get_row("SELECT count( 1 ) AS total_record
				FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
				WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
				lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
				(sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp().") AND
				sales.`total` > 0 AND lineitems.`payment_to` =  'Vendor'", OBJECT);
	
	
	//print date("Y-M-d  H:i:s","1383264000");
	//print date("Y-M-d  H:i:s","1383868799");
	
	$number_of_sales = $query->total_record;
	if (empty($number_of_sales))
	{
		$number_of_sales = "0";
	}
		
	//$row = $wpdb->get_row("select SUM(sale_amount) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'", OBJECT);
	
	$row = $wpdb->get_row("SELECT SUM( lineitems.`total` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.`purchase_id` = sales.`id` AND
				lineitems.`total` > 0 AND
				sales.status = 'ACT' AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
			      (sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp().") AND
			      lineitems.`payment_to` = 'Vendor' ", OBJECT );
	
	$total_sales = $row->total;
	if (empty($total_sales))
	{
		$total_sales = "0.00";
	}
	
	//$row = $wpdb->get_row("select SUM(payment) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'", OBJECT);
	
	$row = $wpdb->get_row("SELECT SUM( lineitems.`seller_comm` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.`purchase_id` = sales.`id` AND
			      lineitems.`total` > 0 AND
			      sales.status = 'ACT' AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND
			      (sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND
			      ".$end_date_tstamp->getTimestamp() .") AND
			      lineitems.`payment_to` = 'Vendor'", OBJECT);
				
	$total_commission = $row->total;
	if (empty($total_commission))
	{
		$total_commission = "0.00";
	}

          $df_commissions = $total_sales-$total_commission;
	
	
	
	
	echo '<h3>Payment Made to '.$_SESSION['user_id'].'</h3>
	<table id="reports" width="300">
	<tbody>';

		/*echo '<tr>';
		echo '<td><strong>'.AFF_TOTAL_CLICKS.' : </strong></td>';
		echo '<td>'.$total_clicks.'</td>';
		echo '</tr>';*/

		echo '<tr>';
		echo '<td><strong>Total Number of Sales : </strong></td>';
		echo '<td>'.$number_of_sales.'</td>';
		echo '</tr>';

		echo '<tr>';
		echo '<td><strong>Total Sales Amount : </strong></td>';
		echo '<td>$'.number_format($total_sales,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>DealFuel Commission : </strong></td>';
		echo '<td>$'.number_format($df_commissions,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>'.$_SESSION['user_id'].' Commissions : </strong></td>';
		echo '<td>$'.number_format($total_commission,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';

								
	echo '</tbody></table>';




}

 ?>
</div>
</div>
</div>
