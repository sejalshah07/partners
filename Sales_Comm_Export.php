<?php
include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}
class SalesCommExport {
	
	var $user_id;
	
	var $action;
	var $updateMsg;
	var $error;
	var $dataErrors = array();

  			
	/**
	 * SalesComm(ission) Export Constructor Function
	 *
	 * @return ReportRun
	 */
  	function SalesCommExport() {
  		$this->user_id = $_REQUEST['user'];
  		//print_r($_SESSION);
  		//echo "user_id".$this->user_id;
  		//echo "<br />user_id SESSION".$_SESSION['user_id'];
	}
	

	/**
	 * Enter description here...
	 *
	 */
	function initSalesCommExport() {
		$this->action = $_REQUEST['action'];
		//echo $this->action;
		switch ($this->action) {
			case 'EXPORT_TO_XLS':
					//echo "<br> before header";
					header('Content-Type: application/vnd.ms-excel;');
					header('Content-type: application/x-msexcel');
					header('Content-Disposition: attachment; filename=export_sales.xls');
					/*header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=file.csv");
header("Pragma: no-cache");
header("Expires: 0");*/
									
					//$this->showHeader();
					//echo "<br> before call to func : ";
					$this->showQuestionResults();
					
					exit;
					break;
	// Begin Added by Arvind on 14-AUG-2013
			case 'EXPORTXML' :
				
					
				header('Content-Type: application/vnd.ms-excel;');
				header('Content-type: application/x-msexcel');
				header('Content-Disposition: attachment; filename=export_' . $this->survey_id . '.xls');
					
				$this->showHeader();
					
				$this->showQuestionResults1();				
					
				exit;
				break;
	// End by Arvind
	
			case 'EXPORTCSV':
				
				header('Content-Description: File Transfer');
				header("Content-type: application/force-download");
				header('Content-Disposition: inline; filename="salesreport.csv"');
				
				$this->showSalesReport();
				break;
			
			default:	
					break;
		}
	}
		

	/**
	 * Enter description here...
	 *
	 */
	private function showHeader() {
		global $wpdb;
		$aff_sales_table = WP_AFF_SALES_TABLE;  
		$query = "select * from 'wp_affiliates_sales_tbl' where refid = '". $this->user_id ."' ORDER BY date DESC LIMIT 20";
		$resultset = $wpdb->get_results($query,OBJECT);
		print_r($resultset);
		if ($resultset) 
		{
			//echo $r->survey_name . " (" . date("m/d/Y", strtotime($r->date_created)) . " - " . date("m/d/Y") . ")\n\n\n";
			echo "SalesComm Name:-".$r->survey_name . "\n\n\n";
			
		} else {
			exit;
		}
	}

	
	/**
	 * Enter description here...
	 *
	 */
	 
	 
	 private function showSalesReport(){
	 
	 global $wpdb, $wp_aff_platform_config;
		$aff_sales_table = WP_AFF_SALES_TABLE;  
		$query = "select * from $aff_sales_table where refid = '".$_SESSION['user_id']."' ORDER BY date DESC LIMIT 20";
		

		//echo "<br>query: ".$query;
		$resultset = $wpdb->get_results($query,OBJECT);
		//print_r($resultset);

	 	if($resultset){
	 	echo "<br>\n".AFF_G_DATE.",".AFF_G_TIME.",";
		echo AFF_S_EARNED.",";
		if(get_option('wp_aff_show_buyer_details_to_affiliates') ||   					$wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1')
		{
			echo AFF_BUYER_DETAILS.",\r";
		}		    
			
		
		//echo "User_id\tUser_Name\tEmail_id\tQuestion\tAnswer\n\n";
		foreach ($resultset as $resultset) {
		
		      	print $resultset->date;
		      	print ",";
		      	print $resultset->time;
		      	print ",";
		      	print $resultset->payment;
		      	print ",";
		      	print $currency;
			    if(!empty($resultset->campaign_id)){
				    echo " (".AFF_CUSTOM_VALUE.': '.$resultset->campaign_id.")";
				}
	
		    	$buyer_txn_details = "";
		      	if($wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->buyer_name.'<br />';
		      	}
		      	if(get_option('wp_aff_show_buyer_details_to_affiliates')){
		      		$buyer_txn_details .= $resultset->buyer_email.'<br />';
		      	}
		    	if($wp_aff_platform_config->getValue('wp_aff_show_txn_id_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->txn_id.'<br />';
		      	}
		      	if(!empty($buyer_txn_details)){
		      		
		      		echo $buyer_txn_details;
		      		
		      	}
		      	echo "\r\n";
		      }
		     
		}
		 else{
		      	echo "No record available";
		} 
	 }
	 
	 
	private function showQuestionResults() {
		
global $wpdb, $wp_aff_platform_config;
		$aff_sales_table = WP_AFF_SALES_TABLE;  
		$query = "select * from $aff_sales_table where refid = '".$_SESSION['user_id']."' ORDER BY date DESC LIMIT 20";
		

		//echo "<br>query: ".$query;
		$resultset = $wpdb->get_results($query,OBJECT);
		//print_r($resultset);
		// echo "\r\n\n".AFF_G_DATE." \t\t".AFF_G_TIME." \t";
		  echo "\r\n\nDATE \tTIME \tYOUR EARNED \tBUYER EMAIL \n";
		 //   echo AFF_S_EARNED." \t";
			if(get_option('wp_aff_show_buyer_details_to_affiliates') || $wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1')
			{
			//	echo AFF_BUYER_DETAILS." \t";
			}		    
			
			echo "\r \n \n \n ";
		  //  echo "User_id\tUser_Name\tEmail_id\tQuestion\tAnswer\n\n";
		foreach ($resultset as $resultset) {
		
		      	echo "\"$resultset->date\" \t";
		      	echo " \t";
		      	echo "\"$resultset->time\" \t";
		      	echo "\t";
		      	echo "\"$resultset->payment\"";
		      	echo " \t";
		      	echo $currency;
			    if(!empty($resultset->campaign_id)){
				    echo " (".AFF_CUSTOM_VALUE.': '.$resultset->campaign_id.")";
				}
	
		    	$buyer_txn_details = "";
		      	if($wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->buyer_name.'<br />';
		      	}
		      	if(get_option('wp_aff_show_buyer_details_to_affiliates')){
		      		$buyer_txn_details .= $resultset->buyer_email.'<br />';
		      	}
		    	if($wp_aff_platform_config->getValue('wp_aff_show_txn_id_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->txn_id.'<br />';
		      	}
		      	if(!empty($buyer_txn_details)){
		      		
		      		echo $buyer_txn_details;
		      		
		      	} 
		      	echo "\r\n";
		      
		}
		
		
	}
	
	// Begin Added by Arvind on 14-AUG-2013 for Export UserInfo
	
	private function showQuestionResults1() {
		
		
		global $wpdb;
		$uid = "SELECT DISTINCT {$wpdb->prefix}sf_survey_results.user_id FROM {$wpdb->prefix}sf_survey_results
		WHERE {$wpdb->prefix}sf_survey_results.survey_id = '$this->survey_id' ";
		
		$tr = $wpdb->get_results($uid);
		echo "User_id \tUser_Name \tEmail_id \tQuestion \tAnswer\n\n";
		$count=0;
		foreach ($tr as $userid) {
			
		//echo "\t$userid->user_id\t\t";
			$count=0;
		$uinfo = "SELECT {$wpdb->prefix}sf_survey_user_information.user_name,{$wpdb->prefix}sf_survey_user_information.email_id FROM {$wpdb->prefix}sf_survey_user_information
		WHERE {$wpdb->prefix}sf_survey_user_information.user_id = '$userid->user_id' ";
		$uinfo1 = $wpdb->get_results($uinfo);
			
		foreach($uinfo1 as $userinfo){
		echo $userid->user_id." \t".$userinfo->user_name." \t".$userinfo->email_id." ";
			
		$select1 = "SELECT {$wpdb->prefix}sf_survey_results.answer,{$wpdb->prefix}sf_survey_results.survey_question_id FROM {$wpdb->prefix}sf_survey_results WHERE {$wpdb->prefix}sf_survey_results.user_id = '$userid->user_id'";
			
		$QA = $wpdb->get_results($select1);
			
		foreach($QA as $qa){
		$count=$count+1;
		$q = $wpdb->get_row("SELECT question FROM {$wpdb->prefix}sf_survey_questions WHERE survey_question_id = '$qa->survey_question_id'");
		if($count==1)
		echo "\"$q->question\" \t\"$qa->answer\" \n";
		else
		echo " \t \t \"$q->question\" \t\"$qa->answer\" \n";
		}
		echo "\n";
			
		}
			
		}
						
		
		
		
	}
	//End by Arvind    
	
} // End SalesComm Export Class


$SalesCommExport = new SalesCommExport();
$SalesCommExport->initSalesCommExport();


?>