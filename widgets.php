<?php include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}

if(!aff_check_security())
{
    aff_redirect('index.php');
    exit;
}
//[rel=lightbox]  
include "header.php"; ?>


 <h3><a href ="#" style="float:left;">Widgets</a></h3>
 <h3><a href"#" style="float: right;"> <?php echo $_SESSION['user_id']; ?> </a></h3>
<?php
echo '<div id="subnav"><li><a href="ads.php">'.AFF_NAV_BANNERS.'</a></li></div>';
echo '<div id="subnav"><li><a href="creatives.php">'.AFF_NAV_CREATIVES.'</a></li></div>';
echo '<div id="subnav"><li><a href="widgets.php">Widgets</a></li></div>';
?>
<!-- Load jQuery Lightbox -->
<script language="JavaScript" type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.lightbox-0.5.pack.js"></script>
<script type="text/javascript">
    $(function() {
        $('#gallery a[rel=lightbox]').lightBox();
    });
</script>
<link rel="stylesheet" type="text/css" href="ads/adstyle.css" />
<script type = "text/javascript" src = "ads/adscript.js"></script>
<br /><br />
<div>

<h4> Select the widget to use on your site: </h4>

<select id = "select-widget">
  <option value="text-widget">Text widget</option>
  <option value="image-widget">Image widget</option>
  <option value="image-widget">Sidebar widget</option>
  <option value="image-widget">Horizontal widget</option>
  <option value="alldeals-widget">All Deals</option>
  <option value="fullpage-widget">Full Page</option>
</select>
<input type="hidden" name="widget" id="widget_hidden" />
<input type="hidden" name="aff_id" id="aff_id" value="<?php echo $_SESSION['user_id']; ?>" />


</div>

<br /><br />
<div class="dealwidget">
<h4>All you have to do is put the given code in the location on your site where you want this text ad to be displayed. It has your affiliate id embedded within. And what’s more, the ad is responsive so you needn’t worry. </h4>
</div>
<br /><br />

<br /><br />
<div id="text-widget">
<h3>Text widget:</h3><br />
</div>
<div id="image-widget">
<h3>Image widget:</h3><br />    
</div>
<div id="sidebar-widget">
<h3>Sidebar widget:</h3><br />
  <div>
    <span>Deals count:</span> <input type="text" id="ads_count" value="" />  
 Default: 3 <br />
  </div>    
</div>

<div id="horizontal-widget">
<h3>Horizontal widget:</h3><br />
  <div>
    <span>Deals count:</span> <input type="text" id="horizontal_ads_count" value="" />  
 Default: 3 <br />
  </div>    
</div>

<div id="all-deals-widget">
<h3>All Deals:</h3><br />
  <div>
    <span>Deals per Page:</span> <input type="text" id="dealsperpage" value="" />  
 Default: 24 <br />
  </div>    
  <div id='categories'> 
  <h4>Show deals only from selected categories:</h4>
    <br>
    <ul class="">
      <li>
      	<label>
          <input class="" type="checkbox" value="bundles" name="df-alldeals-chkbox[]" /> Bundles
        </label>
      </li>
      <li>
        <label>
          <input class="" type="checkbox" value="dealclub-special" name="df-alldeals-chkbox[]" /> DealClub Special
        </label>
      </li>
      <li>
        <label>
          <input class="" type="checkbox" value="ebooks" name="df-alldeals-chkbox[]" /> eBooks
        </label>
      </li>
      <li>
        <label>
          <input class="" type="checkbox" value="ecourses" name="df-alldeals-chkbox[]" />eCourses
        </label>
      </li>
       <li>
        <label>
          <input class="" type="checkbox" value="freebies" name="df-alldeals-chkbox[]" />Freebies
        </label>
      </li>
      <li>
        <label>
          <input class="" type="checkbox" value="graphics-2" name="df-alldeals-chkbox[]" /> Graphics
        </label>
      </li>
      <li>
        <label>
          <input class="" type="checkbox" value="plugins" name="df-alldeals-chkbox[]" /> Plugins
        </label>
      </li>
     <li>
        <label>
          <input class="" type="checkbox" value="seo-2" name="df-alldeals-chkbox[]" /> SEO
        </label>
      </li>
      <li>
        <label>
          <input class="" type="checkbox" value="software" name="df-alldeals-chkbox[]" /> Software
        </label>
      </li>
       <li>
        <label>
          <input class="" type="checkbox" value="templates-and-themes" name="df-alldeals-chkbox[]" />Templates and Themes
        </label>
      </li>
      <li>
        <label>
          <input class="" type="checkbox" value="wordpress-2" name="df-alldeals-chkbox[]" /> WordPress
        </label>
      </li>
    </ul>
  </div>
</div>

<div id="fullpage-widget">
<h3>Full Page:</h3><br />
  
</div>

<br /><br />

<textarea id='dealadcode' cols="60" rows="15"></textarea>

<br /><br />
<div>
<h3>Preview:</h3>
</div>
<div id = "text-widget-preview">
	<div id="df-text-ads-widget"></div>
	<style>#df-text-ads-widget a{font-family:sans serif;}</style>
	<script type="text/javascript">
		(function() {var dfWidgetUrl = "http://dealfuel.com/partners/ads/deal-text-ads.js";
		var tscript = document.createElement("script"), tstyle = document.getElementsByTagName("script")[0];
		tscript.type = "text/javascript";
		tscript.async = true;
		tscript.src = dfWidgetUrl;
		tstyle.parentNode.insertBefore(tscript, tstyle);})();
		var df_aff_id = jQuery('#aff_id').val();
	</script>
</div>
<div id = "image-widget-preview">
	<div id="df-ads-widget"></div>
	<style>#df-ads-widget a{font-family:sans serif;}</style>
	<script type="text/javascript">
		(function() {var dfWidgetUrl = "http://dealfuel.com/partners/ads/deal-ads.js";
		var tscript = document.createElement("script"), tstyle = document.getElementsByTagName("script")[0];
		tscript.type = "text/javascript";
		tscript.async = true;
		tscript.src = dfWidgetUrl;
		tstyle.parentNode.insertBefore(tscript, tstyle);})();
		var df_aff_id = jQuery('#aff_id').val();
	</script>
</div>

<!-- added by Sneha -->
<div id="horizontal-widget-preview">
	<div id="df-horizontal-widget"></div>
	<style>#df-horizontal-widget a{font-family:sans serif;}</style>
	<script type="text/javascript">
		(function() {var dfWidgetUrl = "http://dealfuel.com/partners/ads/deal-horizontal-ads.js";
		var sbscript = document.createElement("script"), sbstyle = document.getElementsByTagName("script")[0];
		sbscript.type = "text/javascript";
		sbscript.async = true;
		sbscript.src = dfWidgetUrl;
		sbstyle.parentNode.insertBefore(sbscript, sbstyle);})();
		var df_aff_id = jQuery('#aff_id').val(); var countofads = jQuery('#horizontal_ads_count').val();
	</script>
</div>

<div id="sidebar-widget-preview">
	<div id="df-sidebar-widget"></div>
	<style>#df-sidebar-widget a{font-family:sans serif;}</style>
	<script type="text/javascript">
		(function() {var dfWidgetUrl = "http://dealfuel.com/partners/ads/deal-sidebar-ads.js";
		var sbscript = document.createElement("script"), sbstyle = document.getElementsByTagName("script")[0];
		sbscript.type = "text/javascript";
		sbscript.async = true;
		sbscript.src = dfWidgetUrl;
		sbstyle.parentNode.insertBefore(sbscript, sbstyle);})();
		var df_aff_id = jQuery('#aff_id').val(); var countofads = jQuery('#ads_count').val();
	</script>
</div>

<div id="alldeals-widget-preview">
	<div id="df-alldeals-widget"></div>
	<div id="pagination_controls"></div>
	<style>#df-alldeals-widget a{font-family:sans serif;}</style>
	<script type="text/javascript">
	(function() {var dfWidgetUrl = "http://dealfuel.com/partners/ads/deal-all-ads.js";
	var adscript = document.createElement("script"), adstyle = document.getElementsByTagName("script")[0];
	adscript.type = "text/javascript";
	adscript.async = true;
	adscript.src = dfWidgetUrl;
	adstyle.parentNode.insertBefore(adscript, adstyle);})();
	var df_aff_id = jQuery('#aff_id').val();
	var page = 1;
	var rpp = jQuery('#dealsperpage').val();
	var category = "all";
	</script>
</div>

<div id="fullpage-widget-preview">
	<div id="df-fullpage-widget"></div>
	<style>#df-fullpage-widget a{font-family:sans serif;}</style>
	<script type="text/javascript">
  	(function() {
      	var dfWidgetUrl = "http://dealfuel.com/partners/ads/deal-fullpage-ads.js";
      	var fpscript = document.createElement("script"), fpstyle = document.getElementsByTagName("script")[0];
      	fpscript.type = "text/javascript";
      	fpscript.async = true;
      	fpscript.src = dfWidgetUrl;
      	fpstyle.parentNode.insertBefore(fpscript, fpstyle);})();
      	var df_aff_id = jQuery('#aff_id').val();
 	</script>
</div>

<?php include "footer.php"; ?>