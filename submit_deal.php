<?php

include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}
//include "./lang/$language";

if(!aff_check_security())
{
    aff_redirect('index.php');
    exit;
}
  
include "header.php"; ?>
<div class="container">
<div class="row">
<div class="table-responsive">
 <h3><a href ="#" style="float:left;">Submit Deal</a></h3>
<h3><a href"#" style="float: right;"> <?php echo $_SESSION['user_id']; ?> </a></h3>




<?php
if(isset($_POST['add_deal_email'])){

	// Create post object
	  $my_post = array(
	     'post_title' => $_POST['add_deal_name'],
	     'post_content' => '',
	     'post_status' => 'draft',
	     'post_author' => 1,
	     'post_type' => 'seller'
	  );
	
	// Insert the post into the database
	$uccess = false;
	if (!($newpost = wp_insert_post($my_post, $wp_error))) {
		echo "$wp_error";
		die('cannot insert post');
	}
	
	if(is_numeric($newpost)){
	 	$success = true;
	}
	 
	$typez = 0;
	 
	 if($_POST['add_deal_type'] == 'affiliate-link'){$typez = 1;}
	 elseif($_POST['add_deal_type'] == 'digital-download'){$typez = 2;}
	 elseif($_POST['add_deal_type'] == 'single-coupon' || $_POST['add_deal_type'] == 'multi-coupon'){$typez = 3;}
	 
	 add_post_meta($newpost, 'coupon_code', $_POST['add_deal_single_code'].$_POST['add_deal_multi_code']);
	 add_post_meta($newpost, 'coupon_website', $_POST['add_deal_website']);
	 add_post_meta($newpost, 'our_price', $_POST['add_deal_n_price']);
	 add_post_meta($newpost, 'current_price', $_POST['add_deal_o_price']);
	 add_post_meta($newpost, 'owner_email', $_POST['add_deal_email']);
	 add_post_meta($newpost, 'owner_name', $_POST['add_deal_user_name']);
	 add_post_meta($newpost, 'coupon_type', $typez);
	 add_post_meta($newpost, 'status', 0);
	 add_post_meta($newpost, 'is_expired', 0);

	if(isset($_POST['add_deal_aff_link'])) {
		add_post_meta($newpost, 'coupon_link', $_POST['add_deal_aff_link']);
	}
	if(isset($_POST['add_deal_single_code'])) {
		add_post_meta($newpost, 'coupon_code', $_POST['add_deal_single_code']);
	}
	if (isset($_POST['add_deal_multi_code'])) {
	 	add_post_meta($newpost, 'no_of_coupon', $_POST['add_deal_no_codes']);
	 	add_post_meta($newpost, 'coupon_code', $_POST['add_deal_multi_code']);
	}
	if(isset($_POST['add_deal_voucher_text'])){
		add_post_meta($newpost, 'voucher_text', $_POST['add_deal_voucher_text']);
	}
	if(isset($_POST['add_deal_dl_text'])){
		add_post_meta($newpost, 'dl_text', $_POST['add_deal_dl_text']);
	}

	$content = '<p>'.$_POST['add_deal_blurb'].'</p>'.
				  //'<p><img src="'.$img_url_1.'" alt=""></p>'.
				  '<p>'.$_POST['add_deal_desc'].'</p>'.
				  //'<p><img src="'.$img_url_2.'" alt=""></p>'.
				  '<p>'.$_POST['add_deal_testimonials'].'</p>'.
				  //'<p><img src="'.$img_url_3.'" alt=""></p>'.
				  '<p>'.$_POST['add_deal_cbio'].'</p>';
	
	// handles basic file upload using wordpress functionality 
	// to do - include more checks
	// bernard 27 mar 2012
	if (isset($_FILES)) {
		for ($i = 0; $i < 4; $i ++) {
			$name = 'add_image'.$i;
			if ($_FILES[$name]['size'][$k] > 2000000) {
				exit('Each image uploaded must be less than 2 Megabytes.');
			}
			elseif (!$_FILES[$name]['error']) {
			  require_once(ABSPATH . "wp-admin" . '/includes/image.php');
	  		require_once(ABSPATH . "wp-admin" . '/includes/file.php');
		  	require_once(ABSPATH . "wp-admin" . '/includes/media.php');
				$id = media_handle_sideload( $_FILES[$name], $newpost, 'seller uploaded image '.$i );
				$content .= '<p><img src="'.wp_get_attachment_url($id).'"/></p>';
			}
		}
	}
	
	// time to update the post
	if (!wp_update_post(array('ID' => $newpost, 'post_content' => $content))) {
		die ('cannot update database');
	}
	else{
	    $affiliates_table_name = WP_AFF_AFFILIATES_TABLE;
            $editingaff = $wpdb->get_row("SELECT * FROM $affiliates_table_name WHERE refid = '".$_SESSION['user_id']."'", OBJECT);
	
	    $to = "deals@dealfuel.com";
	    $subject = "A new Deal is Submitted.";
	    $message = $_POST['add_deal_name']." \r\n\n ". $_POST['add_deal_blurb'] ;
	    $headers = "From: ". $_SESSION['user_id'] ."  <".$editingaff->email."> \r\n";
	    wp_mail( $to, $subject, $message, $headers, $attachments );
	  // mail( $to, $subject, $message, $headers, $attachments );
	}
}

?>

<?php //get_header(); ?>
<!--a href="<?php bloginfo('url'); ?>" class="back-to-home">Return to Dealfuel</a-->

<div  class="" >
	<?php if(!$success) : ?>
	<p class="hmsend">Have your very own deals that you'd like to see on Dealfuel? Fill out the form below, and jump the queue straight to the approval process.</p><br/>
	<?php else : ?>
		<p class="hmsend">
			Thanks for submitting your deal! We've been notified there is a new one in the database and will go check it out ASAP. You'll receive and email if we accept it.
		</p>
	<?php endif; ?>
<!-- contact -->
<?php //if ( have_posts() ) : ?>
<?php// while ( have_posts() ) : the_post(); ?>

<?php
  global $wpdb;
  $affiliates_table_name = WP_AFF_AFFILIATES_TABLE;
  $editingaff = $wpdb->get_row("SELECT * FROM $affiliates_table_name WHERE refid = '".$_SESSION['user_id']."'", OBJECT);

?>
<script language="JavaScript" type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/jquery.validate.js"></script>
<script>
    $(document).ready(function(){
    $.validator.addMethod("username", function(value, element) {
        return this.optional(element) || /^[a-z0-9\_]+$/i.test(value);
    }, "Username must contain only letters, numbers, or underscore.");

    $("#add_deal").validate();
});
</script>
    	 
    	<form method="post" action="<?php the_permalink(); ?>" id="add_deal" enctype="multipart/form-data" align="center">
    		   		
    		<table cellpadding="10" cellspacing="10" border="0" align="center" id="reports">
	    		<tr>
	    			<td><label for="add_deal_user_name">Your Name: </label></td>
    				<td><input type="text" name="add_deal_user_name" id="add_deal_user_name"  value="<?php echo $editingaff->refid; ?>" readonly/></td>
    			</tr>
    			
    			<tr height="15px"></tr>
    			
    			<tr>
    			 <td><label for="add_deal_email">Your Email: </label></td>
    			 <td><input type="text" name="add_deal_email" id="add_deal_email"  value="<?php echo $editingaff->email; ?>" readonly /></td>
    			</tr>
    			
    			<tr height="15px"></tr>
    			
    			<tr>
	    			<td><label for="add_deal_website">Your Website:</label></td>
	    			<td><input type="text" name="add_deal_website" id="add_deal_website"  value="" /></td>
	    		</tr>
    			
    			<tr height="15px"></tr>
    			
    			
    			<tr>
    			   <td><label for="add_deal_name">Deal Title:<br/>(Max. 70 chars including spaces)</label></td>
    				<td><input type="text" name="add_deal_name" id="add_deal_name"  value="" /></td>
				</tr>
				
				<tr height="15px"></tr>
				
				<tr>
					<td><label for="add_deal_o_price">Original Price:</label></td>
					<td><input type="text" name="add_deal_o_price" id="add_deal_o_price"  value="" /></td>
				</tr>
				
				<tr height="15px"></tr>
				
				<tr>
					<td><label for="add_deal_n_price">Discounted Price:</label></td>
					<td><input type="text" name="add_deal_n_price" id="add_deal_n_price"  value="" /></td>
    			</tr>
    			
    			<tr height="15px"></tr>
    			
    			<tr>
	    			<td><label for="add_deal_blurb">Intro blurb:<br/>(Max. 425 chars)</label></td>
	    			<td><textarea name="add_deal_blurb" id="add_deal_blurb"></textarea></td>
				</tr>				
				
				<tr height="15px"></tr>
			
				<tr>	
					<td><label for="add_deal_desc">What customers will receive:<br/>(Max. 1,075 chars)</label></td>
					<td><textarea name="add_deal_desc" id="add_deal_desc"></textarea></td>
				</tr>
				
				<tr height="15px"></tr>
				
				<tr>
					<td><label for="add_deal_testimonials">Testimonials for company/products<br><small>(at least 3):</small></label></td>
					<td><textarea name="add_deal_testimonials" id="add_deal_testimonials"></textarea></td>
				</tr>
				
				<tr height="15px"></tr>
				
				<tr>
					<td><label for="add_deal_cbio">Company Bio:</label></td>
					<td><textarea name="add_deal_cbio" id="add_deal_cbio"></textarea></td>
    			</tr>
    			
    			<tr height="15px"></tr>
    			
    			<tr>
	    			<td><label>Featured Image<br>(480px by 326px, 2MB)</label></td>
	    			<td><input type="file" name="add_image0"/></td>
	    		</tr>
    			
    			<tr height="15px"></tr>
    			
    			<tr>
	    			<td><label>Secondary Image #1<br>(max 600px wide, 2MB)</label></td>
	    			<td><input type="file" name="add_image1"></td>
    			</tr>
    			<tr height="15px"></tr>
    			<tr>
    				<td><label>Secondary Image #2<br>(max 600px wide, 2MB)</label></td>
    				<td><input type="file" name="add_image2"></td>
    			</tr>
    			<tr height="15px"></tr>
    			
    			<tr>
    				<td><label>Secondary Image #3<br>(max 600px wide, 2MB)</label></td>
    				<td><input type="file" name="add_image3"></td>
    			</tr>
    			
    			<tr height="15px"></tr>	
    			
    			<tr>
    			<td><label for="">Deal type: </label></td>
    			<td><select name="add_deal_type" id="add_deal_type">
    					<option>Select a deal type...</option>
    					<option value="affiliate-link">Affiliate Link</option>
    					<option value="digital-download">Digital Download</option>
    					<option value="single-coupon">Single Coupon</option>
    					<option value="multi-coupon">Multiple Coupons</option>
    				</select></td>
    			</tr>
    			
    			<tr height="15px"></tr>
    			
    			<tr>
    			<li class="hide" id="otf">
    				<p class="single-coupon"><label>Coupon</label>
    					<input type="text" name="add_deal_single_code" id="add_deal_single_code"></p>
    				<p class="multi-coupon"><label>Coupons<br></label>
    					<input name="add_deal_no_codes" id="add_deal_no_codes" placeholder="no. coupons"><br><br>
    					<textarea name="add_deal_multi_code" id="add_deal_multi_code" placeholder="Coupon Codes (separate with commas)"></textarea><br><br>
    					</p>
    				<p class="multi-coupon single-coupon"><textarea name="add_deal_voucher_text" placeholder="Coupon download instructions"></textarea></p>
    				<p class="digital-download"><label>Digital Download File</label>
							<!--
    					<input type="file" name="add_deal_dl_file" id="add_deal_dl_file"></p>
							-->
							<textarea name="add_deal_dl_text" placeholder="please provide a link to the digital download or instructions on how to download the file."></textarea>
						</p>
    				<p class="affiliate-link"><label>Affiliate Link</label>
    					<input type="text" name="add_deal_aff_link" id="add_deal_aff_link"></p>
    			</tr>
    			
    			<tr height="15px"></tr>
    			
    			
    			<tr> 
                          <td><label>Accept Terms & Coditions:</label></td>
                           <td><textarea name="Terms & Condition" id="add_deal_terms" readonly>I hearby confirm that the deal is not being sold for a lower price currently. &#13;&#10; I also confirm that it is not going to be sold in the next 8 weeks for a lower price on any other site. </textarea>
                           </td>
               </tr>
    			
    			<tr height="15px"></tr>
               
               <tr>
                   <td><label></label></td>
                  <td>
                  	<font face="Verdana, Arial, Helvetica, sans-serif" font-size="14px" color="#000000">
                  	
                              <input id = "terms_cond" type="checkbox" name="accept_terms" value="1" class = "required"/> * I agree to Terms of Service agreement.
                              </font>
           		</td>
                              	

             </tr>
    		
    			<tr height="15px"></tr>		
    		 
    		 <tr>
    			<td><label for="add_deal_comments">General Comments:</label></td>
    			<td><textarea name="add_deal_comments" id="add_deal_comments"></textarea></td>
    		 </tr>		
 			
 				<tr height="15px"></tr>
 			 
 			 <tr>
 				    <td colspan="2"><input type="submit" id="add_deal_submit"></td>
 				
 			 </tr>
 			 <tr height="45px"></tr>
 
    		</table>
    		
    		
    		<!-- <ul class="form_ul">
    			<li><p><label for="add_deal_user_name">Your Name: </label>
    				<input type="text" name="add_deal_user_name" id="add_deal_user_name"  value="<?php echo $editingaff->refid; ?>" readonly/></p></li>
    			
    			<li><p><label for="add_deal_email">Your Email: </label>
    				<input type="text" name="add_deal_email" id="add_deal_email"  value="<?php echo $editingaff->email; ?>" readonly /></p></li>
    			
    			<li><p><label for="add_deal_website">Your Website:</label>
    				<input type="text" name="add_deal_website" id="add_deal_website"  value="" /></p></li>
    			
    			<li><p><label for="add_deal_name">Deal Title:<br/>(Max. 70 chars including spaces)</label>
    				<input type="text" name="add_deal_name" id="add_deal_name"  value="" /></p></li>
				
				<li><p><label for="add_deal_o_price">Original Price:</label>
					<input type="text" name="add_deal_o_price" id="add_deal_o_price"  value="" /></p></li>
				
				<li><p><label for="add_deal_n_price">Discounted Price:</label>
					<input type="text" name="add_deal_n_price" id="add_deal_n_price"  value="" /></p></li>
    			
    			<li><p><label for="add_deal_blurb">Intro blurb:<br/>(Max. 425 chars)</label>
    				<textarea name="add_deal_blurb" id="add_deal_blurb"></textarea></p></li>
				
				<li><p><label for="add_deal_desc">What customers will receive:<br/>(Max. 1,075 chars)</label>
					<textarea name="add_deal_desc" id="add_deal_desc"></textarea></p></li>
				
				<li><p><label for="add_deal_testimonials">Testimonials for company/products<br><small>(at least 3):</small></label>
					<textarea name="add_deal_testimonials" id="add_deal_testimonials"></textarea></p></li>
				
				<li><p><label for="add_deal_cbio">Company Bio:</label>
					<textarea name="add_deal_cbio" id="add_deal_cbio"></textarea></p></li>
    			
    			<li><p><label>Featured Image<br>(280px by 190px, 2MB)</label>
    				<input type="file" name="add_image0"/></p></li>
    			
    			<li><p><label>Secondary Image #1<br>(max 400px wide, 2MB)</label>
    				<input type="file" name="add_image1"></p></li>
    			
    			<li><p><label>Secondary Image #2<br>(max 400px wide, 2MB)</label>
    				<input type="file" name="add_image2"></p></li>
    			
    			<li><p><label>Secondary Image #3<br>(max 400px wide, 2MB)</label>
    				<input type="file" name="add_image3"></p></li>
    			
    			<li><p><label for="">Deal type: </label>
    				<select name="add_deal_type" id="add_deal_type">
    					<option>Select a deal type...</option>
    					<option value="affiliate-link">Affiliate Link</option>
    					<option value="digital-download">Digital Download</option>
    					<option value="single-coupon">Single Coupon</option>
    					<option value="multi-coupon">Multiple Coupons</option>
    				</select></p>
    			</li>
    			<li class="hide" id="otf">
    				<p class="single-coupon"><label>Coupon</label>
    					<input type="text" name="add_deal_single_code" id="add_deal_single_code"></p>
    				<p class="multi-coupon"><label>Coupons<br></label>
    					<input name="add_deal_no_codes" id="add_deal_no_codes" placeholder="no. coupons"><br><br>
    					<textarea name="add_deal_multi_code" id="add_deal_multi_code" placeholder="Coupon Codes (separate with commas)"></textarea><br><br>
    					</p>
    				<p class="multi-coupon single-coupon"><textarea name="add_deal_voucher_text" placeholder="Coupon download instructions"></textarea></p>
    				<p class="digital-download"><label>Digital Download File</label>
							<!--
    					<input type="file" name="add_deal_dl_file" id="add_deal_dl_file"></p>
							-->
							<!-- <textarea name="add_deal_dl_text" placeholder="please provide a link to the digital download or instructions on how to download the file."></textarea>
						</p>
    				<p class="affiliate-link"><label>Affiliate Link</label>
    					<input type="text" name="add_deal_aff_link" id="add_deal_aff_link"></p>
    			</li>
    			
    			 <li> 
                           <p><label>Accept Terms & Coditions:</label>
                              <textarea name="Terms & Condition" id="add_deal_terms" readonly>I hearby confirm that the deal is not being sold for a lower price currently. &#13;&#10; I also confirm that it is not going to be sold in the next 8 weeks for a lower price on any other site. </textarea>
                           </p>
                           <p>
                              <label></label>
                           <b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000">
                              <input id = "terms_cond" type="checkbox" name="accept_terms" value="1" class = "required"/> * I agree to Terms of Service agreement.
                              </font></b>
                           </p>
                         </li>
    			
    			<li><p><label for="add_deal_comments">General Comments:</label>
    				<textarea name="add_deal_comments" id="add_deal_comments"></textarea></p></li>
    			<li><input type="submit" id="add_deal_submit"></li>
    			
    			
    		   </ul>-->
    	</form>
   



<!--  CONTENT AREA END -->
</div>
</div>
</div>
</div>
<?php //get_footer(); ?>
