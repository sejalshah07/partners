<?php
include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}
//include "./lang/$language";

if(!aff_check_security())
{
    aff_redirect('index.php');
    exit;
}
  
include "header.php"; ?>
<div class="container">
<div class="row">
<div class="table-responsive">
 <h3><a href ="#" style="float:left;">PAYMENT HISTORY</a></h3>
<h3><a href"#" style="float: right;"> <?php echo $_SESSION['user_id']; ?> </a></h3>
<!-- <img src="images/payments_icon.jpg" alt="Payment History Icon" /> -->

<?php
    $currency = get_option('wp_aff_currency');
    $aff_payouts_table = WP_AFF_PAYOUTS_TABLE; 
    $aff_sales_table = WP_AFF_SALES_TABLE;  
    
    //$sales_row = $wpdb->get_row("SELECT SUM( lineitems.`total` ) AS total FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales WHERE lineitems.`purchase_id` = sales.`id` AND lineitems.`seller_name` = '".$_SESSION['user_id']."'", OBJECT);
	//sales without paypal charges
	//$row = $wpdb->get_row("SELECT SUM( lineitems.`seller_comm` ) AS total FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales WHERE lineitems.total > 0 AND sales.status = 'ACT' AND lineitems.`purchase_id` = sales.`id` AND lineitems.`seller_name` = '".$_SESSION['user_id']."' ", OBJECT);
	
    $query = $wpdb->get_row("SELECT count( 1 ) AS total_record
				FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
				WHERE sales.status = 'ACT' AND
				    lineitems.`purchase_id` = sales.`id` AND
				    lineitems.`seller_name` = '".$_SESSION['user_id']."' AND lineitems.`total` > 0");
    
    $number_of_sales = $query->total_record;
    if (empty($number_of_sales))
    	 	$number_of_sales = "0";
    
    
    
	$row = $wpdb->get_row("SELECT SUM( lineitems.`seller_comm` ) AS total, SUM( lineitems.`total` ) AS total_sales
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.total > 0 AND
			      sales.status = 'ACT' AND
			      lineitems.`purchase_id` = sales.`id` AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' ", OBJECT);
	
	$sales_commission = ($row->total != '' ? $row->total : '0');
	$total_sales = ($row->total_sales != '' ? $row->total_sales : '0');
	
	$row = $wpdb->get_row("SELECT SUM( sales.`processor_fee`  ) / 2 AS C2D_charges
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.total > 0 AND
			      sales.status = 'ACT' AND
			      lineitems.`payment_to` = 'DealFuel' AND
			      lineitems.`purchase_id` = sales.`id` AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' ", OBJECT);
	
	$sales_commission_charges = ($row->C2D_charges != '' ? $row->C2D_charges : '0');
	
	//if (empty($total_commission))
	//{
	//	$total_commission = "0.00";
	//}
	
    echo " &nbsp; <h4>All Time Payment Summary</h4>";
	$sales_net = $sales_commission - $sales_commission_charges;
	echo "&nbsp; <table id = 'reports'>";
	echo "<tr>";
	/*echo "<td><strong>Number of Product Sales:</strong></td>";
	echo "<td><strong>".number_format($number_of_sales, 2)."</strong></td>";
	echo "</tr>";
	echo "<td><strong>Total Product Sales Amount : </strong></td>";
	echo "<td>$".number_format($total_sales, 2)."</strong></td>";
	echo "<td>".$currency."</strong></td>";
	echo "</tr>";
	echo "<tr>";*/
	echo "<td><strong>Sales Commission:</strong><br> (after adjusting Paypal fees)</td>";
	echo "<td>$".number_format($sales_net, 2)."</strong></td>";
	echo "<td>".$currency."</strong></td>";
	echo "</tr>";
	echo "<tr>";
	/*echo "<td><strong>Paypal Fees</strong></td>";
	echo "<td><strong>$".number_format($sales_commission_charges, 2)."</strong></td>";
	echo "<td>".$currency."</strong></td>";
	echo "</tr>";*/
	
	echo "<tr><td><strong>Affiliates Commission:</strong></td>";
	
	$aff_row = $wpdb->get_row("select SUM(payment) AS total from $aff_sales_table aff, `wp_getdpd_sales` sales where aff.txn_id = sales.id AND sales.status = 'ACT' AND refid = '".$_SESSION['user_id']."' AND type = 'affiliate'", OBJECT);
	$aff_commission = ($aff_row->total != '' ? $aff_row->total : '0');
	
	//manual aff commissions
	$aff_row = $wpdb->get_row("select SUM(payment) AS total from $aff_sales_table aff where aff.txn_id = 'manual' AND refid = '".$_SESSION['user_id']."' AND type = 'affiliate'", OBJECT);
	$aff_commission_manual = ($aff_row->total != '' ? $aff_row->total : '0');
	
	$aff_commission = $aff_commission + $aff_commission_manual ;
	
	echo "<td>";
	print "$".number_format($aff_commission, 2); 
	print "</td><td>".$currency."</td></tr>"; 
	
	$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND `payout_payment` > 0 ", OBJECT);
	$total_payments_D2V = ($row->total != '' ? $row->total : '0');

	
	$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND `payout_payment` < 0", OBJECT);
	$total_payments_V2D = ($row->total != '' ? $row->total : '0');
	
	//$row = $wpdb->get_row("select SUM(payout_payment) AS total from `wp_affiliates_payouts_tbl` where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date'", OBJECT);
        $row = $wpdb->get_row("SELECT SUM( lineitems.`total` ) AS total FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales WHERE sales.status = 'ACT' AND lineitems.total > 0 AND lineitems.`purchase_id` = sales.`id` AND lineitems.`seller_name` = '".$_SESSION['user_id']."' AND lineitems.`payment_to` = 'Vendor'", OBJECT);
	$total_payments_C2V = ($row->total != '' ? $row->total : '0');
	
	
	//$total_payments_net =   $sales_commission + $aff_commission - $total_payments_D2V - $total_payments_C2V + (-1) * $total_payments_V2D;
	$total_payments_net =   $sales_net + $aff_commission - $total_payments_D2V - $total_payments_C2V + (-1) * $total_payments_V2D;
	
		echo '<tr>';
		echo '<td><strong>DealFuel to '.$_SESSION['user_id'].' Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_D2V,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>'.$_SESSION['user_id'].' to Dealfuel Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_V2D,2).'</td>';
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
		
		echo '<tr>';
		echo '<td><strong>Customer to '.$_SESSION['user_id'].' Payments : </strong></td>';
		echo '<td>$'.number_format($total_payments_C2V,2).'</td>'; 
		echo '<td>'.$currency.'</td>';
		echo '</tr>';
                
		

/*added by Sneha*/
		
		$start_date = "2011-01-01";
		$start_date_tstamp = new DateTime($start_date);
		
		//$month_start=date('Y-m-d', strtotime(date('Y-m')." -1 month"));
		$end_date_cur=date('Y-m-d H:i:s', strtotime(date('Y-m')." -1 month -1day 23:59:59"));
		$end_date_tstamp = new DateTime($end_date_cur);
		
		//echo $end_date_cur;
		$row = $wpdb->get_row("SELECT SUM( lineitems.`seller_comm` ) AS total
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.total > 0 AND
			      sales.status = 'ACT' AND
			      lineitems.`purchase_id` = sales.`id` AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND 
				(sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND "
				.$end_date_tstamp->getTimestamp().")", OBJECT);
		
		
		$sales_commission_cur = ($row->total != '' ? $row->total : '0');
		//echo $sales_commission_cur;
		
		$row = $wpdb->get_row("SELECT SUM( sales.`processor_fee`  ) / 2 AS C2D_charges
			      FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			      WHERE lineitems.total > 0 AND
			      sales.status = 'ACT' AND
			      lineitems.`payment_to` = 'DealFuel' AND
			      lineitems.`purchase_id` = sales.`id` AND
			      lineitems.`seller_name` = '".$_SESSION['user_id']."' AND 
				(sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND "
				.$end_date_tstamp->getTimestamp().")", OBJECT);
		
		$sales_commission_charges_cur = ($row->C2D_charges != '' ? $row->C2D_charges : '0');
		
		//echo $sales_commission_charges_cur;
		$sales_net_cur = $sales_commission_cur - $sales_commission_charges_cur;
		
		$total_payments_net_cur =   $sales_net_cur + $aff_commission - $total_payments_D2V - $total_payments_C2V + (-1) * $total_payments_V2D;
		//$total_payments_net_cur =   $sales_net_cur + $aff_commission - $total_payments_D2V - $total_payments_C2V + (-1) * $total_payments_V2D;
		//echo $total_payments_net_cur;
		//$paypal_masspay_charge=($total_payments_net_cur * (1.96/100))<=50 ? ($total_payments_net_cur * (1.96/100)) : 50;
		if($total_payments_net_cur>0)
			$paypal_masspay_charge=($total_payments_net_cur * (1.96/100))<=50 ? ($total_payments_net_cur * (1.96/100)) : 50;
		else
			$paypal_masspay_charge=0;
		$net_cur_masspay= $total_payments_net_cur - $paypal_masspay_charge;
		
		echo "<tr><td><strong>Total Payment Due:</strong></td>";
		echo " <td>$". number_format($total_payments_net, 2)."</td><td>$currency</td></tr>";
		
		echo "<tr><td><strong>Current Month Payment Due:</strong></td>";
		echo " <td><strong>$". number_format($net_cur_masspay, 2)."</strong></td><td><strong> $currency</strong></td></tr>";
		
		/*end adding by SNeha*/
echo "</table>";




payments_history();

/*echo '<strong>';  
print "<br><br>".AFF_P_TOTAL.": ";
 
$row = $wpdb->get_row("select SUM(payout_payment) AS total from $aff_payouts_table where refid = '".$_SESSION['user_id']."'", OBJECT);

print ($row->total != '' ? number_format($row->total,2) : '0');
print " "; 
print $currency; 
print "<br><br>";
echo '</strong>';
*/
include "footer.php";  

function payments_history()
{

    include ("reports.php");
	
	$currency = get_option('wp_aff_currency');
	global $wpdb;
	
    if (isset($_POST['info_update']))
    {
    	$start_date = (string)$_POST["start_date"];
    	$end_date = (string)$_POST["end_date"];
        echo '<h4>';
        echo AFF_P_DISPLAYING_PAYOUTS_HISTORY.' <font style="color:#222">'.$start_date.'</font> '.AFF_AND.' <font style="color:#222">'. $end_date;
        echo '</font></h4>';
		        	
		$curr_date = (date ("Y-m-d"));		

		$aff_payouts_table = WP_AFF_PAYOUTS_TABLE;   
		$wp_aff_payouts = $wpdb->get_results("select * from $aff_payouts_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date'",OBJECT);
		
		if ($wp_aff_payouts)
		{		
		    print "<table id='reports'>";
		    echo "<TR><TH>Name</TH>";
		    echo "<TH>Date</TH>";
		    echo "<TH>Amount</TH>";
		    echo "<TH>Comments</TH>";
		    echo "<TH></TH></TR>";
		    
		    foreach ($wp_aff_payouts as $resultset) 
		    {
		        print "<TR>";
		      	print "<td class='reportscol col1'>";
		      	print $resultset->refid;
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print $resultset->date;
		      	print "</TD>";
		      	//print "<td class='reportscol'>";
		      	//print $resultset->time;
		      	//print "</TD>";
		      	print "<td class='reportscol'>";
		      	print "$ ";
		      	print $resultset->payout_payment;
		      	print " ";
		      	print $currency; 
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print $resultset->comments;
		      	print "</TD>";
		      	print "</TR>";
		    }
		    print "</TABLE>";
		}
		else
		{
			echo "<br><br><font face=arial>No Payments Found";
		}
    		
	}
	else
	{
		$aff_payouts_table = WP_AFF_PAYOUTS_TABLE;   
		$resultset = $wpdb->get_results("select * from $aff_payouts_table where refid = '".$_SESSION['user_id']."' ORDER BY date and time ",OBJECT);
		
		if ($resultset) 
		{
			echo '<strong>';
			//echo "<br><br>".AFF_P_LAST_20_PAYMENTS;
			echo "<br><br>Showing All Payments that you received (including any adjustment entries)";
			echo '</strong>';
			print "<br><br>";
				
		    //print "<table id='reports'>";
		    //echo "<TR><TH>".AFF_G_DATE."</TH><TH>".AFF_G_TIME."</TH>";
		    //echo "<TH>Comments</TH><TH>".AFF_P_PAYMENT."</TH></TR
		    
		    print "<table id='reports'>";
		    echo "<TR><TH>Name</TH>";
		    echo "<TH>Date</TH>";
		    echo "<TH>Amount</TH>";
		    echo "<TH>Comments</TH>";
		    echo "<TH></TH></TR>";
		    
		    
		    foreach ($resultset as $resultset) 
		    {
			print "<TR>";
		      	print "<td class='reportscol col1'>";
		      	print $resultset->refid;
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print $resultset->date;
		      	print "</TD>";
		      	//print "<td class='reportscol'>";
		      	//print $resultset->time;
		      	//print "</TD>";
		      	print "<td class='reportscol'>";
		      	print "$ ";
		      	print $resultset->payout_payment;
		      	print " ";
		      	print $currency; 
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print $resultset->comments;
		      	print "</TD>";
		      	print "</TR>";
		    }
		    print "</TABLE>";
		}
		else
		{
			echo "<br><br><font face=arial>No Payments Received";
		}		
	}	
}
?>
 </div>
</div>
</div>
