
<?php

include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}

$page_meta_title = "DealFuel Partner Center";
//$page_meta_title = get_option('wp_aff_site_title') ." - ". AFF_PORTAL;
define('AFF_META_TITLE', $page_meta_title);
include "header.php";
?>
<div id="left-content">
<?php 
if(aff_check_security())
{
    aff_redirect('members_only.php');
    exit;
} ?>

    <?php $wp_aff_index_title = $wp_aff_platform_config->getValue('wp_aff_index_title'); ?>

    
	<div id = "full-promo-image">
		<img src = "images/promotional_banner1.png" width = "95%"/>
	</div>
	
	
	<div class="clear"></div>


</div>
<div id="right-content">
	<div id = "login-form">
		 <!-- Start Login Form -->
      <form action="login.php" method="post" name="logForm" id="logForm" >

        <table width="60%" border="0" cellpadding="4" cellspacing="4" class="loginform">
          <tr> 
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr> 
            <td width="28%"><img src="images/user_icon.png" /> Partner ID<?php //echo AFF_USERNAME; ?></td>
            <td width="72%"><input name="userid" type="text" class="required" id="txtbox" size="25"></td>
          </tr>
          <tr>
            <td><img src="images/password_icon.png" /> <?php echo AFF_PASSWORD; ?></td>
            <td><input name="password" type="password" class="required password" id="txtbox" size="25"></td>
          </tr>
          <tr> 
            <td colspan="2"><div align="center">
                <input name="remember" type="checkbox" id="remember" value="1">
                <?php echo AFF_REMEMBER_ME; ?></div></td>
          </tr>
          <tr> 
            <td colspan="2"> <div align="center"> 
                <p> 
                  <input name="wpAffSadoLogin" class="button" type="submit" id="doLogin3" value="<?php echo AFF_LOGIN_BUTTON_LABEL; ?>">
                </p>
                <p><img src="images/register.png" /> <a style="color:#CC0000;" href="register.php"> Partner Sign up<?php //echo AFF_AFFILIATE_SIGN_UP_LABEL; ?></a><font color="#EEE">
                  |</font> <img src="images/forgot_pass.png" /> <a href="forgot.php"><?php echo AFF_FORGOT_PASSWORD_LABEL; ?></a></p>
              </div></td>
          </tr>
        </table>

      </form>
      <!-- End Login Form -->
	</div>
	
</div>

<?php include "footer.php"; ?>