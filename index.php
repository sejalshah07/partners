<?php
include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}

$page_meta_title = "DealFuel Partner Center";
//$page_meta_title = get_option('wp_aff_site_title') ." - ". AFF_PORTAL;
define('AFF_META_TITLE', $page_meta_title);
include "header.php";
?>
<!-- Added in 22dec 20114 by sushma -->
<div class="container text-center">
<div class="row">
<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 displaytable" id="left-content">
<?php 
if(aff_check_security())
{
    aff_redirect('members_only.php');
    exit;
} ?>

    <?php $wp_aff_index_title = $wp_aff_platform_config->getValue('wp_aff_index_title'); ?>

    <h3 class="title"><?php echo ("$wp_aff_index_title"); ?></h3>

    <div id="aff-box-body">

    <?php
    $wp_aff_index_body_tmp = $wp_aff_platform_config->getValue('wp_aff_index_body');//get_option('wp_aff_index_body');
    $wp_aff_index_body = html_entity_decode($wp_aff_index_body_tmp, ENT_COMPAT, "UTF-8");
    //echo $wp_aff_index_body; 
    echo $wp_aff_index_body ;
     ?>

    </div>

    <!--div id="aff-box-content"> 

    <img src="images/user_signup.png" class="center" alt="Affiliate Sign up icon" />
    <div id="aff-box-action">
    <div style="float: left;">
        <a href="register.php"><img src="images/signup_round_40.png" /></a>
    </div>
    <div class="action-head"><a href="register.php"><?php echo AFF_SIGN_UP; ?></a></div>
    <div class="action-tag"><a href="register.php"><?php echo AFF_SIGN_UP_CLICK; ?></a></div>
    </div>

    <img src="images/login_icon_128.png" class="center" alt="Affiliate Login icon" />
    <div id="aff-box-action">
    <div style="float: left;">
        <a href="login.php"><img src="images/login_icon_round_48.png" /></a>
    </div>
        <div class="action-head"><a href="login.php"><?php echo AFF_LOGIN; ?></a></div>
        <div class="action-tag"><a href="login.php"><?php echo AFF_LOGIN_CLICK; ?></a></div>
    </div>
    </div--> 
    

<div class="clear"></div>


</div><!-- left container end -->



<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 displaytable text-center" id="right-content">
	<div  id="right_top">
		<!-- <p>Login</p> -->
		<div id = "login-form">
		 <form action="login.php" method="post" name="logForm" id="logForm" >
		 <table style="width:100%" border="0" cellpadding="2" cellspacing="2" class="loginform">
          <tr> 
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr> 
            <td width="35%"><img src="images/user_icon.png" /> Partner ID<?php //echo AFF_USERNAME; ?></td>
            <td width="65%"><input name="userid" type="text" class="required" id="txtbox" size="25"></td>
          </tr>
          <tr>
            <td><img src="images/password_icon.png" /> <?php echo AFF_PASSWORD; ?></td>
            <td><input name="password" type="password" class="required password" id="txtbox" size="25"></td>
          </tr>
          <tr> 
            <td colspan="2"><div align="center">
                <input name="remember" type="checkbox" id="remember" value="1">
                <?php echo AFF_REMEMBER_ME; ?></div></td>
          </tr>
          <tr> 
            <td colspan="2"> <div align="center"> 
                <p> 
                  <input name="wpAffSadoLogin" class="button" type="submit" id="doLogin3" value="<?php echo AFF_LOGIN_BUTTON_LABEL; ?>"/>
                </p>
                <p><img src="images/register.png" /> <a style="color:#CC0000;" href="register.php"> Partner Sign up<?php //echo AFF_AFFILIATE_SIGN_UP_LABEL; ?></a><font color="#EEE">
                  |</font> <img src="images/forgot_pass.png" /> <a href="forgot.php"><?php echo AFF_FORGOT_PASSWORD_LABEL; ?></a></p>
              </div></td>
          </tr>
        </table>
		</form>
		</div><!-- login-form -->
	</div><!-- right_top -->
	
	<div id="right_bottom">
		 <div id = "df_promo_img">
			<a href = "promote.php">
				<img src = "images/img_trans.gif" width = "250px"/>
				<br />
				<span> 10 Ways to Promote DealFuel !!!</span>
			</a>
		</div>
	</div>

</div><!-- Right content end -->
</div><!-- row end -->
</div><!--  Container end-->
<?php include "footer.php"; ?>
