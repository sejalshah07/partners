
</div> <!-- closing of container open in header.php file -->
</div>
<?php
global $smof_data;
if ($smof_data['footer'] == 1 && tt_getmeta('hidefooter') != '1') {
    $layout = isset($smof_data['footer_layout']) ? $smof_data['footer_layout'] : 3;
    switch ($layout) {
        case 1:
            $col = 1;
            $percent = array(
                'col-xs-12 col-sm-12 col-md-12 col-lg-12');
            break;
        case 2:
            $col = 2;
            $percent = array(
                'col-xs-12 col-sm-6 col-md-6 col-lg-6',
                'col-xs-12 col-sm-6 col-md-6 col-lg-6');
            break;
        case 3:
            $col = 3;
            $percent = array(
                'col-xs-12 col-sm-12 col-md-6 col-lg-6',
                'col-xs-12 col-sm-6 col-md-3 col-lg-3',
                'col-xs-12 col-sm-6 col-md-3 col-lg-3');
            break;
        case 4:
            $col = 3;
            $percent = array(
                'col-xs-12 col-sm-6 col-md-3 col-lg-3',
                'col-xs-12 col-sm-6 col-md-3 col-lg-3',
                'col-xs-12 col-sm-12 col-md-6 col-lg-6 pull-right');
            break;
        case 5:
            $col = 3;
            $percent = array(
                'col-xs-12 col-sm-12 col-md-4 col-lg-4',
                'col-xs-12 col-sm-12 col-md-4 col-lg-4',
                'col-xs-12 col-sm-12 col-md-4 col-lg-4');
            break;
        case 6:
            $col = 4;
            $percent = array(
                'col-xs-12 col-sm-3 col-md-3 col-lg-3',
                'col-xs-12 col-sm-3 col-md-3 col-lg-3',
                'col-xs-12 col-sm-3 col-md-3 col-lg-3',
                'col-xs-12 col-sm-3 col-md-3 col-lg-3');
            break;
        default:
            $col = 4;
            $percent = array(
                'col-xs-12 col-sm-6 col-md-3 col-lg-3',
                'col-xs-12 col-sm-6 col-md-3 col-lg-3',
                'col-xs-12 col-sm-6 col-md-3 col-lg-3',
                'col-xs-12 col-sm-6 col-md-3 col-lg-3');
            break;
    }
    ?>
    
    <div class="container-fluid">
			 
                	<div class="container">
                    	
                			<div class="row bottompad">
	                            <?php if (!S2MEMBER_CURRENT_USER_IS_LOGGED_IN){ ?>
	                            	<div class="col-md-6 col-sm-12 col-xs-12">
										<h3 class="subscribe text-left">Subscribe to Get Cool Tech Deals!!</h3>
	                                </div>
	                                
	                                <form role="form" action="#" method="post" id="newsletter-signup-form" class="newsletter-signup-form sub_form">
	                                	   <div class="col-md-4 col-sm-8 col-xs-12 paddtop1">
					                     	<input type="text" placeholder="Enter email" id="cm-sub-email" class="form-control1 sub_email" name="cm_sub_email">
										  </div>
		                                <div class="col-md-2 col-sm-4 col-xs-6 paddtop1">
											<button class="btn btn-success btn-lg btn-block green-effect sub_submit" name="cm_sub_submit" id="cm-sub-submit" data-target=".subscribe_modal" type="button">
												SUBSCRIBE
											</button>
		                                </div> 
		                                
		                                  <input type="hidden" name="popup_signup" id="popup_signup" value="false"/>
								         <input type="hidden" name="data-nonce" id="data-nonce" value="<?php echo wp_create_nonce("add_subscriber_nonce")?>"/>
								           
		                            </form>
					    <div class="modal fade subscribe_message-sm" tabindex="-1" role="dialog" aria-labelledby="subscribeLabel" aria-hidden="true">
						<div class="modal-dialog modal-sm">
							<div class="modal-content">
                                                               <div class="modal-header subscribe_modal_header">
                                                                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                      <h4 class="modal-title" id="subscribeTitle"></h4>
                                                                </div><!--modal-header-->
								<div class="modal-body" id="subscribeBody"></div>
							</div><!--modal-content-->
						</div><!--modal-dialog modal-sm-->
                                	    </div>
		                            
		                            
		                       <?php }
		                       else{ 
									global $current_user;
									$user_roles = $current_user->roles;?>
			                       	<div class="col-md-12">
			                       		<?php 
			                       		  $role = "";
			                       			switch($user_roles[0]){
													case 'subscriber' : $role= 'DealFuel Free Member'; break;
													case 's2member_level0': $role= 'DealClub Free Member'; break;
													case 's2member_level1': $role= 'DealClub Monthly Member'; break;
													case 's2member_level2': $role= 'DealClub Quarterly Member';break;
													case 's2member_level3': $role= 'DealClub Annual Member';break;
													default: $message="Thank you for being with DealFuel!!";
													
			                       			}
			                       		  if(current_user_is("s2member_level0") || current_user_is("s2member_level1") || current_user_is("s2member_level2") || current_user_is("s2member_level3")){?>
				                       		 <h3 class="subscribe text-center">You are <?php echo $role;?></h3>
				                       		 
				                       	<?php }	 else{ ?>
				                       		<h3 class="subscribe text-center"><?php echo $message;?></h3>
				                       	<?php } ?>
				                       		 
			                       	</div>
		                       	
		                       <?php } ?>
	                       </div><!-- row -->
	                 </div> <!-- container -->
             
</div>


    <!-- Start Footer -->
    <footer id="footer" class="clearfix">
        <div class="container">
            <div class="row">
                <?php 
                $footer_custom_color = '';
                $prefix = 'footer';
                for ($i = 1; $i <= $col; $i++) {
                    if(isset($smof_data['use_footer_column_color']) && $smof_data['use_footer_column_color'] == 1) {
                        $footer_custom_color = "style='background-color:".$smof_data['footer_' . $i . '_bg_color']."'";
                        $prefix = 'footer_' . $i;
                    }
                    echo "<div class='footer_widget_container ".$percent[$i - 1]." "; text_brightness_indicator($prefix); echo "' $footer_custom_color>";
                    dynamic_sidebar('sidebar_metro_footer' . $i);
                    ?>
                    <?php 
                    echo '</div>';
                } ?>
            </div><!-- End row -->
        </div><!-- End container -->
    </footer>
    <!-- End Footer -->
<?php } ?>


<?php if (isset($smof_data['sub_footer']) && $smof_data['sub_footer'] == 1 && tt_getmeta('hidesubfooter') != '1') { ?>
    <!-- Start sub footer -->
    <div id="sub_footer" class="sub_footer <?php text_brightness_indicator('sub_footer'); ?>">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-6 col-sm-6">
                    <?php sub_footer_content($smof_data['sub_footer_left']); ?>
                </div>
                <div class="col-xs-12 col-md-12 col-lg-6 col-sm-6 align_right">
                    <?php sub_footer_content($smof_data['sub_footer_right'], 'right'); ?>
                </div>
            </div><!-- End row -->
        </div><!-- End container -->
    </div>
    <!-- End sub footer -->
<?php } ?>
<?php tt_trackingcode(); ?>
<?php wp_footer(); ?>

    </div><!-- end .wrapper -->

    <span class="gototop_footer gototop">


        <i class="icon-angle-up"></i>
    </span>
</body>
</html>
