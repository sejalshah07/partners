<?php
include_once ('misc_func.php');
if(!isset($_SESSION)){@session_start();}
//include "./lang/$language";

if(!aff_check_security())
{
    aff_redirect('index.php');
    exit;
}
  
include "header.php"; ?>
<div class="container">
<div class="row">
<div class="table-responsive">
<h3><a href ="#" style="float:left;">SALES</a></h3>
<h3><a href ="#" style="float: right;"><?php echo $_SESSION['user_id'] ;?></a></h3>
<!-- <img src="images/sales_icon.jpg" alt="Sales Icon" /> -->

<?php
$currency = get_option('wp_aff_currency');
$aff_sales_table = WP_AFF_SALES_TABLE;  

sales_by_deal($aff_sales_table);
//sales_history($aff_sales_table);

echo '<strong>';  
/*print "<br><br>".AFF_S_TOTAL.": ";
 
$row = $wpdb->get_row("select SUM(payment) AS total from $aff_sales_table where refid = '".$_SESSION['user_id']."'", OBJECT);
$total = round($row->total,2);
print ($total != '' ? $total : '0');
print " "; 
print $currency; 
print "<br><br>";
echo '</strong>';*/

include "footer.php";

/* ----------------- Changes made By Dinesh on 14th November, 2013 ------------------------- */

function sales_by_deal($aff_sales_table)
{

   // include ("reports.php");
	
	$currency = get_option('wp_aff_currency');
	global $wpdb,$wp_aff_platform_config;
	
   		/*$query = "SELECT DISTINCT
			(lineitems.`product_id`,
			lineitems.`price`),lineitems.`product_name`,  
			FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
		    WHERE lineitems.`total` > 0 AND lineitems.`purchase_id` = sales.`id` AND
		        lineitems.`seller_name` = '".$_SESSION['user_id']."'";*/
		        
		$query = "SELECT 	lineitems.`product_id`,
					lineitems.`price`,
					replace(lineitems.`product_name`,
					'(DealClub)','') AS product_name,
					SUM( lineitems.`total` ) AS total,
					SUM( IF(lineitems.`payment_to` = 'DealFuel', (sales.`processor_fee` / 2),0) ) AS c2d_charges,
					COUNT(lineitems.`total`) AS count,
					SUM( lineitems.`seller_comm` ) AS commission,
					seller_name as seller_name
			    FROM `wp_getdpd_sales_lineitems` AS lineitems,
				    `wp_getdpd_sales` AS sales 
			    WHERE lineitems.`total` > 0 AND
				    sales.`status` = 'ACT' AND
				    lineitems.`purchase_id` = sales.`id` AND
				    lineitems.`seller_name` = '".$_SESSION['user_id']."' 
			    GROUP By lineitems.`product_id`";
		//echo $query;
			    
			    
		
		
		/*$sql = "SELECT SUM( lineitems.`total` ) AS total, COUNT(lineitems.`total`) AS count, SUM( lineitems.`seller_comm` ) AS commission, seller_name 
			FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
			WHERE 	sales.`status` = 'ACT' AND
				lineitems.`total` > 0 AND
				lineitems.`purchase_id` = sales.`id` AND
				lineitems.`product_id` = ". $resultset->product_id */
		
		
			//echo $query;
			//die();

		
		$deals_array = $wpdb->get_results($query,OBJECT);
		
		if ($deals_array) 
		{
			echo '<strong>';
			//echo "<br><br><font face=arial>".AFF_S_SALES;
			echo "<br><br><font face=arial>Sales from your Deals";
			echo '</strong>';
			
			print "<br><br>";
			//echo "<div id = 'exporttoCSV' >[ <a href='Sales_Comm_Export.php?action=EXPORT_TO_XLS' target=\"_blank\"><b>Export to CSV</b></a> ] &nbsp;</div>";			
		    print "<table id='reports'>";
		    echo "<TR><th>Deal Name</th><TH>Number Of Sales #</TH>";
		    echo "<th>Value of Sales $</th>";
		    echo "<th>Paypal Fees $</th>";
		    echo "<th>Commission $</th>";
		    echo "<th>Export</th>";
		    echo "</TR>";
			
		    
	    
		    foreach ($deals_array as $deal) 
		    {
		        
			//$wp_aff_payouts = $wpdb->get_results("SELECT * FROM $payouts_table WHERE date BETWEEN '$start_date' AND '$end_date'", OBJECT);
			/*$sql = "SELECT SUM( lineitems.`total` ) AS total, COUNT(lineitems.`total`) AS count, SUM( lineitems.`seller_comm` ) AS commission, seller_name 
				FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
				WHERE 	sales.`status` = 'ACT' AND
				lineitems.`total` > 0 AND
				lineitems.`purchase_id` = sales.`id` AND
				lineitems.`product_id` = ". $resultset->product_id
				*/
				//." AND lineitems.`price` = ". $resultset->price
				;
				
		//echo $sql;			
		//$result = $wpdb->get_row($sql, OBJECT);
		$sales_amount = $deal->total;
		$sales_number = $deal->count;
		$commission = $deal->commission;
		$c2d_charges = $deal->c2d_charges;
		$commission_net = $commission - $c2d_charges;

		$nonce = wp_create_nonce( 'dealfuel_csv_nonce'.$deal->seller_name );
		$export_link = "http://".$_SERVER['SERVER_NAME']."/wp-content/plugins/wp-affiliate-platform/export_dealfuel.php?_wpnonce={$nonce}&p1=2&p2=".$deal->seller_name."&p5=".$deal->product_id."";
			
		
			print "<TR>";
		      	print "<td class='reportscol'>";
		      	print $deal->product_name;
		      	print "</TD>";		    
		        
		      	print "<td class='reportscol'>";
		      	print $sales_number;
		      	print "</TD>";		    
		        
			print "<td class='reportscol'>";
		      	print "$".number_format($sales_amount,0);
		      	print "</TD>";		    
		        
			print "<td class='reportscol'>";
		      	print "$".number_format($c2d_charges,2);
		      	print "</TD>";		    
		      	
			print "<td class='reportscol'>";
		      	print "$".number_format($commission_net,2);
		      	print "</TD>";		    
		      	
			print "<td class='reportscol'>";
			echo '<a href = "'.$export_link.'" target = "_blank">csv</a>';
		      	print "</TD>";		    
		      	
			print "</TR>";
		    }
		    print "</TABLE>";
		}
		else
		{
			echo "<br /><p>No Deals Found.</p>";
		}	

		
	
}



/* ------------------------End By Dinesh on 14th November,2013 ----------------------------- */


function sales_history($aff_sales_table)
{

    include ("reports.php");
	
	$currency = get_option('wp_aff_currency');
	global $wpdb,$wp_aff_platform_config;
	
    if (isset($_POST['info_update']))
    {
    	$start_date = (string)$_POST["start_date"];
    	$end_date = (string)$_POST["end_date"];
    	$curr_date = (date ("Y-m-d"));
        echo '<h4>';
        echo AFF_S_DISPLAYING_SALES_HISTORY.' <font style="color:#222;">'.$start_date.'</font> '.AFF_AND.' <font style="color:#222;">'. $end_date;
        echo '</font></h4>';

		if(WP_AFFILIATE_SHOW_EPC_DATA_TO_AFFILIATES === '1'){//show EPC data
        	//TODO - make a settings option for this
			//http://support.google.com/affiliatenetwork/publisher/bin/answer.py?hl=en&answer=107746
			$affiliates_clickthroughs_table = WP_AFF_CLICKS_TBL_NAME;
			$sales_table = WP_AFF_SALES_TBL_NAME;
			$query = $wpdb->get_row("SELECT count(*) as total_record FROM $affiliates_clickthroughs_table WHERE refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date'", OBJECT);
			$total_clicks = $query->total_record;
			if (empty($total_clicks)){$total_clicks = "0";}
	    	
			$row = $wpdb->get_row("select SUM(payment) AS total from $sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date'", OBJECT);
			$total_commission = $row->total;
			if (empty($total_commission)){$total_commission = "0.00";}
			$aff_epc_data1 = (float)$total_commission;
			$aff_epc_data2 = (int)$total_clicks;
			if($aff_epc_data2<1){$aff_epc_data2=1;}
			$aff_epc_data = ($aff_epc_data1/$aff_epc_data2)*100;
			$aff_epc = number_format($aff_epc_data,2);

			echo "<strong>".AFF_SALES_COMMISSION_EARNED."</strong>".$total_commission.' '.$currency.'<br />';
			echo '<strong>'.AFF_SALES_EARNING_PER_CLICK.'</strong>'.$aff_epc.' '.$currency.'<br /><br />';
	    }
	    
		/*$wp_aff_sales = $wpdb->get_results("select * from $aff_sales_table where refid = '".$_SESSION['user_id']."' AND date BETWEEN '$start_date' AND '$end_date' AND type = 'seller'",OBJECT);
		if ($wp_aff_sales)
		{	
		   // echo "<div id = 'exporttoCSV' >[ <a href='Sales_Comm_Export.php?action=EXPORT_TO_XLS' target=\"_blank\"><b>Export to CSV</b></a> ] &nbsp;</div>";	
		    print "<table id='reports'>";
		    echo "<TR><th>Transaction Id</th><TH>".AFF_G_DATE."</TH><TH>".AFF_G_TIME."</TH>";
		    echo "<th>Item Name</th><th>Sale Amount</th><TH>".AFF_S_EARNED."</TH>";
			if(get_option('wp_aff_show_buyer_details_to_affiliates') || $wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1')
			{
				echo "<th>".AFF_BUYER_DETAILS."</th>";
			}		    
		    echo "<th>Type</th></TR>";
		    
		    foreach ($wp_aff_sales as $resultset) 
		    {
 			print "<TR>";
		      	print "<td class='reportscol'>";
		      	print $resultset->txn_id;
		      	print "</TD>";		    
		        
		      	print "<td class='reportscol col1'>";
		      	print $resultset->date;
		      	print "</TD>";
		      	print "<td class='reportscol col1'>";
		      	print $resultset->time;
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print $resultset->item_name;
		      	print "</TD>";	
		      	print "<td class='reportscol'>";
		      	print $resultset->sale_amount;
		      	print "</TD>";	
		      	print "<td class='reportscol'>";
		      	print round($resultset->payment,2);
		      	print " ";
		      	print $currency; 
		    	if(!empty($resultset->campaign_id)){
				    echo " (".AFF_CUSTOM_VALUE.': '.$resultset->campaign_id.")";
				}		      	
		      	print "</TD>";
		    	$buyer_txn_details = "";
		      	if($wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->buyer_name.'<br />';
		      	}
		      	if(get_option('wp_aff_show_buyer_details_to_affiliates')){
		      		$buyer_txn_details .= $resultset->buyer_email.'<br />';
		      	}
		    	if($wp_aff_platform_config->getValue('wp_aff_show_txn_id_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->txn_id.'<br />';
		      	}
		      	if(!empty($buyer_txn_details)){
		      		echo "<td class='reportscol'>";
		      		echo $buyer_txn_details;
		      		echo "</td>";
		      	}
		      	print "<td class='reportscol'>";
		      	print $resultset->type;
		      	print "</TD>";
		      			      	
		      	print "</TR>";
		    }
		    print "</TABLE>";*/
		    
		    // Modification Made by  Dinesh on 6th November, 2013
	
			    $start_date_tstamp = new DateTime($start_date);
//echo "start:".$start_date_tstamp->getTimestamp();
//echo "--";
$end_date_tstamp = new DateTime($end_date." 23:59:59");
//echo "end: ".$end_date_tstamp->getTimestamp();
	
	
		    
		 $query = "SELECT lineitems.`purchase_id`, sales.`created_at`, lineitems.`product_name`, lineitems.`total`, lineitems.`seller_comm`,
		 sales.`buyer_email`, sales.`buyer_firstname`, sales.`buyer_lastname`, sales.`timestamp`, `sales.status` , lineitems.`payment_to` FROM `wp_getdpd_sales_lineitems`
		  AS lineitems, `wp_getdpd_sales` AS sales WHERE lineitems.total > 0 AND lineitems.`purchase_id` = sales.`id` AND lineitems.`seller_name` = '".$_SESSION['user_id']."'
		 AND sales.`created_at` BETWEEN ".$start_date_tstamp->getTimestamp()." AND ".$end_date_tstamp->getTimestamp()."  ORDER BY sales.`timestamp` DESC";
		//echo "$query";
		$resultset = $wpdb->get_results($query,OBJECT);
		
		if ($resultset) 
		{
			echo '<strong>';
			echo "<br><br><font face=arial>".AFF_S_SALES;
			echo '</strong>';
			//echo "<br><br>".AFF_S_SHOWING_20;
			echo "<br><br> Showing All Sales";
			print "<br><br>";
			//echo "<div id = 'exporttoCSV' >[ <a href='Sales_Comm_Export.php?action=EXPORT_TO_XLS' target=\"_blank\"><b>Export to CSV</b></a> ] &nbsp;</div>";			
		    print "<table id='reports'>";
		   echo "<TR><th>Purchase Id</th><TH>Date & Time</TH>";
		    echo "<th>Product Name</th><th>Sale Amount</th><TH>".AFF_S_EARNED."</TH>";
			if(get_option('wp_aff_show_buyer_details_to_affiliates') || $wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1')
			{
				echo "<th>".AFF_BUYER_DETAILS."</th>";
			}		    
		    echo "<th>Payment To</th><th>Status</th></TR>";
		    
		    foreach ($resultset as $resultset) 
		    {
		       print "<TR>";
		      	print "<td class='reportscol'>";
		      	print $resultset->purchase_id;
		      	print "</TD>";		    
		        
		      	print "<td class='reportscol col1'>";
		      	print date("Y-M-d  H:i:s",$resultset->created_at);
		      	print "</TD>";
		      	/*print "<td class='reportscol col1'>";
		      	print $resultset->time;
		      	print "</TD>";*/
		      	print "<td class='reportscol'>";
		      	print $resultset->product_name;
		      	print "</TD>";	
		      	print "<td class='reportscol'>";
		      	print $resultset->total;
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print $resultset->seller_comm;
		      	print " ";
		      	print $currency;
			    if(!empty($resultset->campaign_id)){
				    echo " (".AFF_CUSTOM_VALUE.': '.$resultset->campaign_id.")";
				}
		      	print "</TD>";
		    	$buyer_txn_details = "";
		      	if($wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->buyer_firstname.' '.$resultset->buyer_lastname.'<br />';
		      	}
		      	if(get_option('wp_aff_show_buyer_details_to_affiliates')){
		      		$buyer_txn_details .= $resultset->buyer_email.'<br />';
		      	}
		    	if($wp_aff_platform_config->getValue('wp_aff_show_txn_id_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->puchase_id.'<br />';
		      	}
		      	if(!empty($buyer_txn_details)){
		      		echo "<td class='reportscol'>";
		      		echo $buyer_txn_details;
		      		echo "</td>";
		      	}
		      	print "<td class='reportscol'>";
		      	print $resultset->payment_to;
		      	print "</TD>";
		      	
			print "<td class='reportscol'>";
		      	print $resultset->status;
		      	print "</TD>";
			
			print "</TR>";
		    }
		    print "</TABLE>";

		    
		    
		}
		else
		{
			echo "<br /><br /><p>".AFF_S_NO_SALES_IN_THIS_PERIOD."</p>";
		}
    		
	}
	else
	{/*	$query = "select * from $aff_sales_table where refid = '".$_SESSION['user_id']."' AND type = 'seller' ORDER BY date DESC LIMIT 20";
		
		$resultset = $wpdb->get_results($query,OBJECT);
		
		if ($resultset) 
		{
			echo '<strong>';
			echo "<br><br><font face=arial>".AFF_S_SALES;
			echo '</strong>';
			echo "<br><br>".AFF_S_SHOWING_20;
			print "<br><br>";
			//echo "<div id = 'exporttoCSV' >[ <a href='Sales_Comm_Export.php?action=EXPORT_TO_XLS' target=\"_blank\"><b>Export to CSV</b></a> ] &nbsp;</div>";			
		    print "<table id='reports'>";
		   echo "<TR><th>Transaction Id</th><TH>".AFF_G_DATE."</TH><TH>".AFF_G_TIME."</TH>";
		    echo "<th>Item Name</th><th>Sale Amount</th><TH>".AFF_S_EARNED."</TH>";
			if(get_option('wp_aff_show_buyer_details_to_affiliates') || $wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1')
			{
				echo "<th>".AFF_BUYER_DETAILS."</th>";
			}		    
		    echo "<th>Type</th></TR>";
		    
		    foreach ($resultset as $resultset) 
		    {
		       print "<TR>";
		      	print "<td class='reportscol'>";
		      	print $resultset->txn_id;
		      	print "</TD>";		    
		        
		      	print "<td class='reportscol col1'>";
		      	print $resultset->date;
		      	print "</TD>";
		      	print "<td class='reportscol col1'>";
		      	print $resultset->time;
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print $resultset->item_name;
		      	print "</TD>";	
		      	print "<td class='reportscol'>";
		      	print $resultset->sale_amount;
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print $resultset->payment;
		      	print " ";
		      	print $currency;
			    if(!empty($resultset->campaign_id)){
				    echo " (".AFF_CUSTOM_VALUE.': '.$resultset->campaign_id.")";
				}
		      	print "</TD>";
		    	$buyer_txn_details = "";
		      	if($wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->buyer_name.'<br />';
		      	}
		      	if(get_option('wp_aff_show_buyer_details_to_affiliates')){
		      		$buyer_txn_details .= $resultset->buyer_email.'<br />';
		      	}
		    	if($wp_aff_platform_config->getValue('wp_aff_show_txn_id_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->txn_id.'<br />';
		      	}
		      	if(!empty($buyer_txn_details)){
		      		echo "<td class='reportscol'>";
		      		echo $buyer_txn_details;
		      		echo "</td>";
		      	}
		      	print "<td class='reportscol'>";
		      	print $resultset->type;
		      	print "</TD>";
		      	print "</TR>";
		    }
		    print "</TABLE>";
		}
		else
		{
			echo "<br /><p>".AFF_S_NO_SALES."</p>";
		}	
		*/
		//------------ Modification by DInesh on 6th November, 2013--------------------
		
		$query = "
		SELECT 	lineitems.`purchase_id`,
			sales.`created_at`,
			lineitems.`product_name`,
			lineitems.`total`,
			lineitems.`seller_comm`,
			sales.`buyer_email`,
			sales.`buyer_firstname`,
			sales.`buyer_lastname`,
			sales.`timestamp`,
			sales.`status`,
			lineitems.`payment_to`
		FROM `wp_getdpd_sales_lineitems` AS lineitems, `wp_getdpd_sales` AS sales
		WHERE lineitems.total > 0 AND lineitems.`purchase_id` = sales.`id` AND
		    lineitems.`seller_name` = '".$_SESSION['user_id']."'
		ORDER BY sales.`timestamp`
		DESC LIMIT 200";
		
		$resultset = $wpdb->get_results($query,OBJECT);
		
		if ($resultset) 
		{
			echo '<strong>';
			echo "<br><br><font face=arial>".AFF_S_SALES;
			echo '</strong>';
			echo "<br><br> Showing Last 200 Sales";
			print "<br><br>";
			//echo "<div id = 'exporttoCSV' >[ <a href='Sales_Comm_Export.php?action=EXPORT_TO_XLS' target=\"_blank\"><b>Export to CSV</b></a> ] &nbsp;</div>";			
		    print "<table id='reports'>";
		   echo "<TR><th>Purchase Id</th><TH>Date & Time</TH>";
		    echo "<th>Product Name</th><th>Sale Amount</th><TH>".AFF_S_EARNED."</TH>";
			if(get_option('wp_aff_show_buyer_details_to_affiliates') || $wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1')
			{
				echo "<th>".AFF_BUYER_DETAILS."</th>";
			}		    
		    echo "<th>Payment To</th><th>Status</th></TR>";
		    
	    
		    foreach ($resultset as $resultset) 
		    {
		       print "<TR>";
		      	print "<td class='reportscol'>";
		      	print $resultset->purchase_id;
		      	print "</TD>";		    
		        
		      	print "<td class='reportscol col1'>";
		      	print date("Y-M-d  H:i:s",$resultset->created_at);
		      	print "</TD>";
		      	/*print "<td class='reportscol col1'>";
		      	print $resultset->time;
		      	print "</TD>";*/
		      	print "<td class='reportscol'>";
		      	print $resultset->product_name;
		      	print "</TD>";	
		      	print "<td class='reportscol'>";
		      	print $resultset->total;
		      	print "</TD>";
		      	print "<td class='reportscol'>";
		      	print number_format($resultset->seller_comm,2);//$resultset->seller_comm;
		      	print " ";
		      	print $currency;
			    if(!empty($resultset->campaign_id)){
				    echo " (".AFF_CUSTOM_VALUE.': '.$resultset->campaign_id.")";
				}
		      	print "</TD>";
		    	$buyer_txn_details = "";
		      	if($wp_aff_platform_config->getValue('wp_aff_show_buyer_details_name_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->buyer_firstname.' '.$resultset->buyer_lastname.'<br />';
		      	}
		      	if(get_option('wp_aff_show_buyer_details_to_affiliates')){
		      		$buyer_txn_details .= $resultset->buyer_email.'<br />';
		      	}
		    	if($wp_aff_platform_config->getValue('wp_aff_show_txn_id_to_affiliates')=='1'){
		      		$buyer_txn_details .= $resultset->puchase_id.'<br />';
		      	}
		      	if(!empty($buyer_txn_details)){
		      		echo "<td class='reportscol'>";
		      		echo $buyer_txn_details;
		      		echo "</td>";
		      	}
		      	print "<td class='reportscol'>";
		      	print $resultset->payment_to;
		      	print "</TD>";
			
			
			print "<td class='reportscol'>";
		      	print $resultset->status;
		      	print "</TD>";
			
		      	print "</TR>";
		    }
		    print "</TABLE>";
		}
		else
		{
			echo "<br /><p>".AFF_S_NO_SALES."</p>";
		}	

		
	}	
}
?>
</div>
</div>
</div>
